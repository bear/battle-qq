package websocket.service.impl;

import java.util.List;

import websocket.constant.Config;
import websocket.dao.UsersDAO;
import websocket.pojo.Friends;
import websocket.pojo.Group;
import websocket.pojo.Users;

/**
 * 用户基本信息的服务类
 * 
 * @author zhc
 * 
 */
public class UserService {

	/**
	 * 校验用户是否存在
	 * 
	 * @param uid
	 *            用户ID
	 * @param password
	 *            用户密码
	 * @return
	 */
	public boolean validate(long uid, String password) {
		UsersDAO usersDAO = new UsersDAO();
		Users user = usersDAO.findUser(uid);
		if (user == null || !user.getPassword().equals(password)) {
			return false;
		}
		return true;
	}

	/**
	 * 根据用户ID来获得用户的信息
	 * 
	 * @param uid
	 *            用户ID
	 * @return
	 */
	public Users getUser(long uid) {
		UsersDAO usersDAO = new UsersDAO();
		Users user = usersDAO.findUser(uid);
		return user;
	}

	/**
	 * 获得一个用户的好友分组
	 * 
	 * @param uid
	 *            用户ID
	 * @return
	 */
	public List<Group> getGroups(long uid) {
		UsersDAO usersDAO = new UsersDAO();
		return usersDAO.getGroups(uid);
	}

	/**
	 * 获得一个分组下的好友信息
	 * 
	 * @param gid
	 *            分组ID
	 * @return
	 */
	public List<Users> getFriends(long gid) {
		UsersDAO usersDAO = new UsersDAO();
		return usersDAO.getFriends(gid);
	}

	/**
	 * 更新一个用户的在线状态
	 * 
	 * @param uid
	 *            用户ID
	 * @param status
	 *            用户状态
	 */
	public void updateStatus(long uid, int status) {
		UsersDAO usersDAO = new UsersDAO();
		usersDAO.updateStatus(uid, status);
	}

	/**
	 * 获得在线好友
	 * 
	 * @param uid
	 *            用户ID
	 * @return
	 */
	public List<Users> getOnlineFriends(long uid) {
		UsersDAO usersDAO = new UsersDAO();
		return usersDAO.findOnlineFirends(uid);
	}

	/**
	 * 校验邮箱是否可用
	 * 
	 * @param mail
	 * @return
	 */
	public boolean validateMail(String mail) {
		UsersDAO usersDAO = new UsersDAO();
		return usersDAO.validateMail(mail);
	}

	public boolean insert(Users user) {
		UsersDAO usersDAO = new UsersDAO();
		return usersDAO.persistence(user);
	}

	/**
	 * 获得一个唯一的UID，需要注意的是这个方法是同步的，因此必须注意死锁
	 * 
	 * @return
	 */
	public static synchronized Long productUniqueNum() {
		UsersDAO usersDAO = new UsersDAO();
		Long maxId = usersDAO.getMaxId();
		if (maxId == 0) {
			maxId = Config.DEF_MAXID;
		}
		Long count = usersDAO.userCount();
		if ((maxId - count) < Config.DEF_AVAIIABLE) {
			maxId = maxId * 10;
		}
		Long uid = (long) (Math.random() * maxId);
		while (usersDAO.uidExist(uid)) {
			uid = (long) (Math.random() * maxId);
		}
		return uid;
	}

	/**
	 * 创建一个默认分组并添加自己为好友
	 * 
	 * @param uid
	 *            用户ID
	 */
	public void createDefGroup(Long uid) {
		Group group = new Group(uid, Config.DEF_GROUP);
		UsersDAO usersDAO = new UsersDAO();
		usersDAO.persistence(group);
		Group g = usersDAO.getGroupByName(uid, Config.DEF_GROUP);
		usersDAO.persistence(new Friends(g.getId(), uid));
	}

	/**
	 * 根据用户昵称或ID来查找用户
	 * 
	 * @param condition
	 *            条件的值
	 * @param page
	 *            页数
	 * @return
	 */
	public List<Users> searchFriendAccurate(String condition, Integer page) {
		UsersDAO usersDAO = new UsersDAO();
		List<Users> userList = usersDAO.list(condition, page);
		return userList;
	}

	/**
	 * 根据用户昵称或ID来查找用户的总数
	 * 
	 * @param condition
	 *            条件的值，ID或昵称
	 * @return
	 */
	public Long searchFriendAccurateTotal(String condition) {
		UsersDAO usersDAO = new UsersDAO();
		Long total = usersDAO.getTotal(condition);
		return total;
	}

	/**
	 * 添加好友的方法
	 * 
	 * @param uid
	 *            好友ID
	 * @param gid
	 *            分组ID
	 * @param mark
	 *            备注，因系统暂无备注，此参数暂无使用
	 */
	public void addFriend(Long uid, Integer gid, String mark) {
		UsersDAO usersDAO = new UsersDAO();
		usersDAO.persistence(new Friends(gid, uid));
	}

	/**
	 * 检测一个用户是否为一个用户的好友
	 * 
	 * @param uid
	 *            用户ID
	 * @param fid
	 *            好友ID
	 * @return
	 */
	public boolean checkFriend(Long uid, Long fid) {
		UsersDAO usersDAO = new UsersDAO();
		return usersDAO.checkFriend(uid, fid);
	}
}
