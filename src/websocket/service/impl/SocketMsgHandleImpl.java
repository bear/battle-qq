package websocket.service.impl;

import org.apache.log4j.Logger;

import websocket.pojo.Message;
import websocket.service.BaseMsgHandleService;
import websocket.service.SocketMsgHandleService;
import websocket.socket.MyWebSocket;

public class SocketMsgHandleImpl implements SocketMsgHandleService {
	private static final Logger logger = Logger
			.getLogger(SocketMsgHandleImpl.class.getName());
	private MyWebSocket socket;
	private BaseMsgHandleService msgService;

	public SocketMsgHandleImpl(MyWebSocket socket) {
		this.socket = socket;
	}

	/**
	 * 从消息中解析出表情，减小数据的传输量
	 */
	@Override
	public String resolveFace(String message) {
		return null;
	}

	@Override
	public void selectHandle(Message message) {
		if (message != null && message.getStatus() != null) {
			// if(!SocketContext.getInstance().isExist(message.getFrom())){
			// return;//如果帐号在上下文中找不到，则发送方非法
			// }
			if (message.getStatus() > 100 && message.getStatus() < 200) {
				msgService = new UserMsgHandleServiceImpl();
				msgService.handle(message, this.socket);
			} else if (message.getStatus() > 200 && message.getStatus() < 300) {
				msgService = new CommonMsgHandleImpl();
				msgService.handle(message, this.socket);
			} else if (message.getStatus() > 300 && message.getStatus() < 400) {
				msgService = new SysMsgHandleServiceImpl();
				msgService.handle(message, this.socket);
			} else {// 消息状态码错误，无法处理
				this._err(message);
			}
		}
	}

	@Override
	public void _err(Message message) {

	}

}
