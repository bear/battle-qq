package websocket.service;

import websocket.pojo.Message;

public interface SocketMsgHandleService {

	public String resolveFace(String message);

	public void selectHandle(Message message);

	public void _err(Message message);

}
