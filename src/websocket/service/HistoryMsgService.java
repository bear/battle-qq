package websocket.service;

import websocket.pojo.Message;

public interface HistoryMsgService {

	public void addHistory(Message message);

	public String getHistory(Long from, Long target,java.sql.Date date);
}
