package websocket.service;

import websocket.pojo.Message;
import websocket.socket.MyWebSocket;

public interface BaseMsgHandleService {

	public void handle(Message message,MyWebSocket socket);
	
	public void sendMsg(Message message);
}
