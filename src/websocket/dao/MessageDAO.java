package websocket.dao;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

import websocket.pojo.History;

/**
 * 聊天消息的持久化的DAO层
 * 
 * @author zhc
 * 
 * 
 */
public class MessageDAO {

	private static final Logger logger = Logger.getLogger(MessageDAO.class
			.getName());

	/**
	 * 插入和一条记录到数据库
	 * 
	 * @param message
	 */
	public void insertHistory(History history) {
		Session session = HibernateSessionFactory.getSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(history);
		} catch (Exception e) {
			logger.error(e.getMessage());
		} finally {
			session.getTransaction().commit();
			session.close();
		}
	}

	/**
	 * 更新一条历史记录
	 * 
	 * @param message
	 */
	public void updateHistory(History history) {
		Session session = HibernateSessionFactory.getSession();
		session.beginTransaction();
		try {
			Query query = session
					.createQuery("update History set record=CONCAT(record,',',?) where id=?");
			query.setText(0, history.getRecord())
					.setInteger(1, history.getId());
			query.executeUpdate();
		} catch (Exception e) {
			logger.error(e.getMessage());
		} finally {
			session.getTransaction().commit();
			session.close();
		}
	}

	/**
	 * 检测两个用户在某一天的聊天记录是否存在，存在返回true，不存在返回false
	 * 
	 * @param uid
	 *            一个用户的ID
	 * @param auid
	 *            另外一个用户的ID
	 * @param date
	 *            记录日期
	 * @return
	 */
	public History exist(Long uid, Long auid, Date date) {
		Session session = HibernateSessionFactory.getSession();
		session.beginTransaction();
		History history = new History();
		try {
			Query query = session
					.createQuery("from History where (user=? and auser=? and date=?) or (auser=? and user=? and date=?) ");
			query.setLong(0, uid).setLong(1, auid).setDate(2, date);
			query.setLong(3, uid).setLong(4, auid).setDate(5, date);
			history = (History) query.uniqueResult();
		} catch (Exception e) {
			logger.error(e.getMessage());
		} finally {
			session.getTransaction().commit();
			session.close();
		}
		return history;
	}

	/**
	 * 查找两个用户在某一段时间内的聊天记录
	 * 
	 * @param user
	 * @param auser
	 * @param begin
	 * @param end
	 * @return
	 */
	public List<History> findHistory(Long user, Long auser,
			java.sql.Date begin, java.sql.Date end) {
		List<History> history = new ArrayList<History>();
		Session session = HibernateSessionFactory.getSession();
		session.beginTransaction();
		try {
			Query query = session
					.createQuery("from History where (user=? and auser=? and date>=? and date<=?) or (auser=? and user=? and date>=? and date<=?) order by date asc ");
			query.setLong(0, user).setLong(1, auser).setDate(2, begin)
					.setDate(3, end).setLong(4, user).setLong(5, auser)
					.setDate(6, begin).setDate(7, end);
			history = query.list();
		} catch (Exception e) {
			logger.error(e.getMessage());
		} finally {
			session.getTransaction().commit();
			session.close();
		}
		return history;
	}
}
