package websocket.constant;

public interface Status {

	/************************* 1码段 *****************************/
	public final int USER_ONLINE = 111;// 发送给用户他的好友上线的提醒
	public final int USER_OFFLINE = 112;// 发送给用户他的好友下线的提醒
	public final int USER_BUSY = 113;
	public final int USER_AWAY = 114;
	public final int USER_INVISIBLE = 115;
	public final int USER_REONLINE = 116;

	public final int SYS_OTHERSPACE = 122;// 帐号在其它地点登录，被强制下线
	/************************* 2码段 *****************************/
	public final int MSG_TEXT = 201;// 表示普通的文本消息
	public final int MSG_SHAKE = 205;// 表示抖动消息
	/************************* 3码段 *****************************/
	public final int SYS_ONLINE = 301;// 用户发给系统表示自己上线
	public final int SYS_OFFLINE = 302;// 用户发给系统表示自己下线
	public final int SYS_BUSY = 303;// 用户发给系统表示自己状态更改为忙碌的消息
	public final int SYS_AWAY = 304;// 用户发给系统表示自己状态更改为离线的消息
	public final int SYS_INVISIBLE = 305;// 用户发给系统表示自己状态更改为隐身的消息
	public final int SYS_REONLINE = 306;// 用户发给系统表示自己状态更改为上线的消息与301状态不同
}
