package websocket.constant;

/**
 * 应用配置常量
 * 
 * @author zhc
 * 
 */
public interface Config {

	/**
	 * 默认分组名称
	 */
	public static final String DEF_GROUP = "我的好友";

	/**
	 * 好友查找每页条数
	 */
	public static final Integer SEARCH_SIZE = 12;

	/**
	 * 默认最大ID，正常情况下只有当数据库为空时才会使用
	 */
	public static final Long DEF_MAXID = 666666L;

	/**
	 * 默认最低可用用户数据量
	 */
	public static final Integer DEF_AVAIIABLE = 1000;
}
