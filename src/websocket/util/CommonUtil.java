package websocket.util;

import org.apache.log4j.Logger;

import websocket.constant.Status;
import websocket.pojo.Message;

import com.jplus.json.JSONException;
import com.jplus.json.JSONObject;

/**
 * 公共的工具类，一些常用的方法均由此类提供
 * 
 * @author zhc
 * 
 */
public class CommonUtil {
	private static final Logger logger = Logger.getLogger(CommonUtil.class
			.getName());

	/**
	 * 将用户聊天中带有时间的字符串格式化为只有日期的java.sql.Date对象
	 * 
	 * @param time
	 * @return
	 */
	public static java.sql.Date parseDate(String time) {
		java.sql.Date date = null;
		if (time != null && time.length() > 10) {
			try {
				date = java.sql.Date.valueOf(time.substring(0, 10));
			} catch (Exception e) {
				logger.error(e.getMessage());
			}
		}
		return date;
	}

	/**
	 * 将JAVABEAN的消息处理为JSON格式
	 * 
	 * @param message
	 *            Message的对象
	 * @return
	 */
	public static String handleMsgToJSON(Message message) {
		return new JSONObject(message).toString();
	}

	/**
	 * 将字符串形式的消息解析为JAVABEAN
	 * 
	 * @param message
	 *            JSON格式的字符串
	 * @return
	 */
	public static Message resolveMessage(String message) {
		Message msg = new Message();
		JSONObject obj = null;
		try {
			obj = new JSONObject(message);
			msg.setStatus(obj.getInt("status"));
			msg.setFrom(obj.getLong("from"));
			msg.setTime(obj.getString("time"));
			msg.setTarget(obj.getLong("target"));
			msg.setMsg(obj.getString("msg"));
		} catch (JSONException e) {// 应返回给用户一个无法处理的消息
			logger.error(e.getMessage());
		}
		return msg;
	}

	public static Integer transformStatus(String sta) {
		try {
			int status = Integer.parseInt(sta);
			if (status < 0 || status > 4) {
				status = 0;
			}
			return status;
		} catch (NumberFormatException e) {
			logger.error(e.getMessage());
			return Status.USER_OFFLINE;
		}
	}
	
}
