package websocket.pojo;

/**
 * Group entity.
 * 
 * @author MyEclipse Persistence Tools
 */

public class Group implements java.io.Serializable {

	private static final long serialVersionUID = 6983821865693409218L;
	private Integer id;
	private Long uid;
	private String groupname;

	// Constructors

	/** default constructor */
	public Group() {
	}

	public Group(Long uid, String groupname) {
		this.uid = uid;
		this.groupname = groupname;
	}

	/** full constructor */
	public Group(Integer id, Long uid, String groupname) {
		this.id = id;
		this.uid = uid;
		this.groupname = groupname;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Long getUid() {
		return this.uid;
	}

	public void setUid(Long uid) {
		this.uid = uid;
	}

	public String getGroupname() {
		return this.groupname;
	}

	public void setGroupname(String groupname) {
		this.groupname = groupname;
	}

}