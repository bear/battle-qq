package websocket.pojo;

import java.util.HashSet;
import java.util.Set;

/**
 * Users entity.
 * 
 * @author MyEclipse Persistence Tools
 */

public class Users implements java.io.Serializable {

	private static final long serialVersionUID = -1978883986388991319L;
	private Long uid;
	private String password;
	private String mail;
	private String sex;
	private String nickname;
	private Integer online;
	private String icon;
	private String signature;
	private Set<Group> groups = new HashSet<Group>();

	// Constructors

	/** default constructor */
	public Users() {
		this.nickname = "暂无";
		this.sex = "男";
		this.online = 0;
		this.icon = "1";
	}

	public Users(Long uid, Integer online) {
		this.uid = uid;
		this.online = online;
	}

	public Users(Long uid, String mail, String sex, String nickname,
			Integer online, String icon, String signature) {
		super();
		this.uid = uid;
		this.mail = mail;
		this.sex = sex;
		this.nickname = nickname;
		this.online = online;
		this.icon = icon;
		this.signature = signature;
	}

	/** full constructor */
	public Users(Long uid, String password, String mail, String sex,
			String nickname, Integer online, String icon, String signature) {
		this.uid = uid;
		this.password = password;
		this.mail = mail;
		this.sex = sex;
		this.nickname = nickname;
		this.online = online;
		this.icon = icon;
		this.signature = signature;
	}

	// Property accessors

	public Long getUid() {
		return this.uid;
	}

	public void setUid(Long uid) {
		this.uid = uid;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMail() {
		return this.mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getSex() {
		return this.sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getNickname() {
		return this.nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public Integer getOnline() {
		return this.online;
	}

	public void setOnline(Integer online) {
		this.online = online;
	}

	public String getIcon() {
		return this.icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getSignature() {
		return this.signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public Set<Group> getGroups() {
		return groups;
	}

	public void setGroups(Set<Group> groups) {
		this.groups = groups;
	}

	@Override
	public String toString() {
		return "nickname:" + this.nickname + ",uid:" + this.uid + ",password:"
				+ this.password;
	}

}