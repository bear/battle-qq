package websocket.pojo;

/**
 * Friends entity.
 * 
 * @author MyEclipse Persistence Tools
 */

public class Friends implements java.io.Serializable {

	private static final long serialVersionUID = -3171945904462051303L;
	private Integer id;
	private Integer gid;
	private Long fuid;

	public Friends() {
	}

	public Friends(Integer gid, Long fuid) {
		this.gid = gid;
		this.fuid = fuid;
	}

	/** full constructor */
	public Friends(Integer id, Integer gid, Long fuid) {
		this.id = id;
		this.gid = gid;
		this.fuid = fuid;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getGid() {
		return this.gid;
	}

	public void setGid(Integer gid) {
		this.gid = gid;
	}

	public Long getFuid() {
		return this.fuid;
	}

	public void setFuid(Long fuid) {
		this.fuid = fuid;
	}

}