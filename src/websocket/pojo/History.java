package websocket.pojo;

import java.sql.Date;

public class History {

	private Integer id;
	private Long user;
	private Long auser;
	private Date date;
	private String record;

	public History() {

	}

	public History( Long user, Long auser, Date date, String record) {
		this.user = user;
		this.auser = auser;
		this.date = date;
		this.record = record;
	}

	public String getRecord() {
		return record;
	}

	public void setRecord(String record) {
		this.record = record;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Long getUser() {
		return user;
	}

	public void setUser(Long user) {
		this.user = user;
	}

	public Long getAuser() {
		return auser;
	}

	public void setAuser(Long auser) {
		this.auser = auser;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

}
