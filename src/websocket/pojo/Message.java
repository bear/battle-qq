package websocket.pojo;

/**
 * 用户消息传输的存储的BEAN
 * 
 * @author zhc
 * 
 */
public class Message {

	/**
	 * 消息状态码
	 */
	private Integer status;
	/**
	 * 消息发送者的ID
	 */
	private Long from;
	/**
	 * 消息接收者的ID
	 */
	private Long target;

	/**
	 * 消息发送时间
	 */
	private String time;
	/**
	 * 消息内容
	 */
	private String msg;

	public Message() {

	}

	public Message(Integer status, Long from, Long target, String time,
			String msg) {
		super();
		this.status = status;
		this.from = from;
		this.target = target;
		this.time = time;
		this.msg = msg;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Long getFrom() {
		return from;
	}

	public void setFrom(Long from) {
		this.from = from;
	}

	public Long getTarget() {
		return target;
	}

	public void setTarget(Long target) {
		this.target = target;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	@Override
	public String toString() {
		StringBuilder str = new StringBuilder();
		str.append("status:").append(this.status).append(",");
		str.append("from:").append(this.from).append(",");
		str.append("time:").append(this.time).append(",");
		str.append("target:").append(this.target).append(",");
		str.append("msg:").append(this.msg);
		return str.toString();
	}

}
