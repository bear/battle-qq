package websocket.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import org.apache.log4j.Logger;
import org.util.digest.EncryptionArithmetic;
import org.zhc.annotation.Action;
import org.zhc.annotation.ActionClass;
import org.zhc.context.ActionContext;

import websocket.service.impl.UserService;

@ActionClass
public class LoginAction {
	private static final Logger logger = Logger.getLogger(LoginAction.class
			.getName());

	private Long uid;
	private String password;

	public Long getUid() {
		return uid;
	}

	public void setUid(Long uid) {
		this.uid = uid;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Action(name = "Login", namespace = "/servlet")
	public String login() {
		UserService service = new UserService();
		PrintWriter out = null;
		try {
			out = ActionContext.getContext(this).getResponse().getWriter();
			if (this.uid != null && this.password != null) {
				EncryptionArithmetic encry = new EncryptionArithmetic(
						EncryptionArithmetic.SHA_256);
				this.password = encry.getDigest(this.password);
				if (service.validate(this.uid, this.password)) {
					out.print(1);
				} else {
					out.print(0);
				}
			}
		} catch (IOException e) {
			logger.error(e.getMessage());
		} finally {
			out.flush();
		}
		return null;
	}
}
