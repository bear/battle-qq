package websocket.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.List;

import org.apache.log4j.Logger;
import org.zhc.annotation.Action;
import org.zhc.annotation.ActionClass;
import org.zhc.context.ActionContext;

import websocket.pojo.Group;
import websocket.pojo.Users;
import websocket.service.HistoryMsgService;
import websocket.service.impl.HistoryMsgServiceImpl;
import websocket.service.impl.UserService;

import com.jplus.json.JSONArray;
import com.jplus.json.JSONObject;

@ActionClass
public class UserMessageAction {
	private static final Logger logger = Logger.getLogger(LoginAction.class
			.getName());

	private Long uid;
	private Long gid;
	private Long fid;
	private Integer days;

	public Long getUid() {
		return uid;
	}

	public void setUid(Long uid) {
		this.uid = uid;
	}

	public Long getGid() {
		return gid;
	}

	public void setGid(Long gid) {
		this.gid = gid;
	}

	public Integer getDays() {
		return days;
	}

	public void setDays(Integer days) {
		this.days = days;
	}

	public Long getFid() {
		return this.fid;
	}

	public void setFid(Long fid) {
		this.fid = fid;
	}

	/**
	 * 获取用户自己的个人信息的方法
	 * 
	 * @return
	 */
	@Action(name = "SelfMessage", namespace = "/servlet",param={"uid"},attribute={})
	public String getSelfMessage() {
		try {
			ActionContext.getContext(this).getResponse()
					.setContentType("text/json");
			PrintWriter out = ActionContext.getContext(this).getResponse()
					.getWriter();
			if (this.uid != null) {
				UserService service = new UserService();
				Users user = service.getUser(this.uid);
				JSONObject json = new JSONObject(user);
				out.print(json.toString());
			} else {
				out.print("error");
			}
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
		return null;
	}

	/**
	 * 获取用户所拥有的分组的方法
	 * 
	 * @return
	 */
	@Action(name = "LoadGroup", namespace = "/servlet",param={"uid"},attribute={})
	public String getGroup() {
		try {
			ActionContext.getContext(this).getResponse()
					.setContentType("text/json");
			PrintWriter out = ActionContext.getContext(this).getResponse()
					.getWriter();
			if (this.uid != null) {
				UserService service = new UserService();
				List<Group> groups = service.getGroups(this.uid);
				JSONArray json = new JSONArray(groups);
				out.print(json.toString());
			} else {
				out.print("error");
			}
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
		return null;
	}

	/**
	 * 获取一个分组下面好友的方法
	 * 
	 * @return
	 */
	@Action(name = "LoadFriends", namespace = "/servlet",param={"gid"},attribute={})
	public String getFriends() {
		try {
			ActionContext.getContext(this).getResponse()
					.setContentType("text/json");
			PrintWriter out = ActionContext.getContext(this).getResponse()
					.getWriter();
			if (this.gid != null) {
				UserService service = new UserService();
				List<Users> friends = service.getFriends(this.gid);
				JSONArray json = new JSONArray(friends);
				out.print(json.toString());
			} else {
				out.print("error");
			}
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
		return null;
	}

	/**
	 * 加载历史记录
	 * 
	 * @return
	 */
	@Action(name = "LoadHistory", namespace = "/servlet",param={"uid","fid","days"},attribute={})
	public String findHistory() {
		HistoryMsgService service = new HistoryMsgServiceImpl();
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DAY_OF_MONTH, 0-this.days);
		java.sql.Date begin = new java.sql.Date(c.getTime().getTime());
		String history = service.getHistory(this.uid, this.fid, begin);
		try {
			ActionContext.getContext(this).getResponse()
					.setContentType("text/json");
			PrintWriter out = ActionContext.getContext(this).getResponse()
					.getWriter();
			out.print(history);
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
		return null;
	}
}
