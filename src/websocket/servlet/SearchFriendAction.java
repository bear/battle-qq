package websocket.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.zhc.annotation.Action;
import org.zhc.annotation.ActionClass;
import org.zhc.context.ActionContext;

import websocket.pojo.Users;
import websocket.service.impl.UserService;

import com.jplus.json.JSONArray;

@ActionClass
public class SearchFriendAction {

	private static final Logger logger = Logger
			.getLogger(SearchFriendAction.class.getName());

	private Integer type;

	private String condition;

	private Integer page;

	private Long uid;

	private Long fid;

	private String mark;

	private Integer gid;

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getCondition() {
		return condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public String getMark() {
		return mark;
	}

	public void setMark(String mark) {
		this.mark = mark;
	}

	public Integer getGid() {
		return gid;
	}

	public void setGid(Integer gid) {
		this.gid = gid;
	}

	public Long getUid() {
		return uid;
	}

	public void setUid(Long uid) {
		this.uid = uid;
	}

	public Long getFid() {
		return fid;
	}

	public void setFid(Long fid) {
		this.fid = fid;
	}

	@Action(name = "SearchFriend", namespace = "/servlet")
	public String search() {
		List<Users> userList = new ArrayList<Users>();
		UserService service = new UserService();
		Long total = 0L;
		if (type == 0) {// 精确查找
			userList = service.searchFriendAccurate(this.condition, this.page);
			total = service.searchFriendAccurateTotal(condition);
		} else {
			// TODO 暂时不开发
		}
		try {
			ActionContext.getContext(this).getResponse()
					.setContentType("text/json");
			PrintWriter out = ActionContext.getContext(this).getResponse()
					.getWriter();
			StringBuilder json = new StringBuilder("{\"total\":" + total
					+ ",\"rows\":");
			if (userList != null) {
				json.append(new JSONArray(userList).toString());
			}
			json.append("}");
			out.print(json);
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
		return null;
	}

	@Action(name = "CheckFriend", namespace = "/servlet", param = { "fid",
			"uid" })
	public String checkFriend() {
		UserService service = new UserService();
		boolean exist = service.checkFriend(uid, fid);
		try {
			PrintWriter out = ActionContext.getContext(this).getResponse()
					.getWriter();
			out.print(exist);
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
		return null;
	}

	@Action(name = "QueryAdd", namespace = "/servlet", param = { "mark", "gid",
			"uid" })
	public String addFriend() {
		UserService service = new UserService();
		service.addFriend(uid, gid, null);
		try {
			PrintWriter out = ActionContext.getContext(this).getResponse()
					.getWriter();
			out.print(1);
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
		return null;
	}

}
