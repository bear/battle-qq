package websocket.servlet;

import javax.servlet.http.HttpServletRequest;

import org.eclipse.jetty.websocket.WebSocket;
import org.eclipse.jetty.websocket.WebSocketServlet;

/**
 * WebSocket的servlet
 * 
 * @author 张浩春
 * 
 */
public class MyWebSocketServlet extends WebSocketServlet {

	private static final long serialVersionUID = 7015253627736845717L;

	@Override
	public WebSocket doWebSocketConnect(HttpServletRequest request,
			String protocol) {
		return new websocket.socket.MyWebSocket();
	}

}
