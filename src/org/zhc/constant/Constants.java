package org.zhc.constant;

public interface Constants {

	public static final String FORWARD = "forward";
	public static final String REDIRECT = "redirect";
	public static final String JSON = "json";
	public static final String INCLUED = "include";
	public static final String GENERAL = "general";
	public static final java.text.SimpleDateFormat dateFormat = new java.text.SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss");
	
	public static final String DEFAULT_CHARSET = "UTF-8";
}
