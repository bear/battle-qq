package org.zhc.po;

public class Namespace implements java.io.Serializable{

	private static final long serialVersionUID = -7984750402802028433L;
	private String namespace;
	private String actionName;

	public Namespace() {

	}

	public Namespace(String namespace, String actionName) {
		this.namespace = namespace;
		this.actionName = actionName;
	}

	public String getNamespace() {
		return namespace;
	}

	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	public String getActionName() {
		return actionName;
	}

	public void setActionName(String actionName) {
		this.actionName = actionName;
	}

	@Override
	public boolean equals(Object obj) {
		Namespace ns = (Namespace) obj;
		if (this.namespace == null || this.actionName == null
				|| ns.getNamespace() == null || ns.getActionName() == null) {
			return false;
		}
		if (this.getNamespace().equals(ns.getNamespace())
				&& this.getActionName().equals(ns.getActionName()))
			return true;
		return false;
	}

}
