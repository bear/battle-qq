package org.zhc.po;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Result implements java.io.Serializable {

	private static final long serialVersionUID = 7967066663300332517L;
	private String value;
	private String type;
	private String location;
	private List<Property> properties = new ArrayList<Property>();
	private Set<String> attributes = new HashSet<String>();
	
	public Result(){
		
	}

	public Result(String value, String type) {
		super();
		this.value = value;
		this.type = type;
	}

	public Result(String value, String type, List<Property> properties) {
		super();
		this.value = value;
		this.type = type;
		this.properties = properties;
	}

	public Result(String value, String type, List<Property> properties,
			Set<String> attributes) {
		super();
		this.value = value;
		this.type = type;
		this.properties = properties;
		this.attributes = attributes;
	}

	public Result(String value, String type, String location) {
		super();
		this.value = value;
		this.type = type;
		this.location = location;
	}

	public Result(String value, String type, String location,
			List<Property> properties) {
		super();
		this.value = value;
		this.type = type;
		this.location = location;
		this.properties = properties;
	}

	public Result(String value, String type, String location,
			List<Property> properties, Set<String> attributes) {
		super();
		this.value = value;
		this.type = type;
		this.location = location;
		this.properties = properties;
		this.attributes = attributes;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<Property> getProperties() {
		return properties;
	}

	public void setProperties(List<Property> properties) {
		this.properties = properties;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Set<String> getAttributes() {
		return attributes;
	}

	public void setAttributes(Set<String> attributes) {
		this.attributes = attributes;
	}

}
