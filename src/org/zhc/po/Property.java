package org.zhc.po;

public class Property {

	private String name;
	private String content;

	public Property() {

	}

	public Property(String name, String content) {
		super();
		this.name = name;
		this.content = content;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Override
	public String toString() {
		return "Property [name=" + name + ", content=" + content + "]";
	}
	
	

}
