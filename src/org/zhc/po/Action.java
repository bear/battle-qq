package org.zhc.po;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Action implements java.io.Serializable {

	private static final long serialVersionUID = 622554610719886452L;
	private String name;
	private String clazz;
	private String method;
	private List<Result> results = new ArrayList<Result>();
	private Set<String> params = new HashSet<String>();
	private Set<String> attributes = new HashSet<String>();

	public Action() {

	}

	public Action(String name, String clazz, String method,
			List<Result> results, Set<String> params, Set<String> attributes) {
		super();
		this.name = name;
		this.clazz = clazz;
		this.method = method;
		this.results = results;
		this.params = params;
		this.attributes = attributes;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getClazz() {
		return clazz;
	}

	public void setClazz(String clazz) {
		this.clazz = clazz;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public List<Result> getResults() {
		return results;
	}

	public void setResults(List<Result> results) {
		this.results = results;
	}

	public Set<String> getParams() {
		return params;
	}

	public void setParams(Set<String> params) {
		this.params = params;
	}

	public Set<String> getAttributes() {
		return attributes;
	}

	public void setAttributes(Set<String> attributes) {
		this.attributes = attributes;
	}

}
