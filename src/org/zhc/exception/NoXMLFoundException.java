package org.zhc.exception;

public class NoXMLFoundException extends RuntimeException{
	
	private static final long serialVersionUID = -1334198605904267893L;

	public NoXMLFoundException(Exception e){
		super(e);
	}
	
	public NoXMLFoundException(String msg){
		super(msg);
	}
}
