package org.zhc.exception;

/**
 * 没有找到对应action的异常，此异常为运行时异常
 * @author 张浩春
 *
 */
public class NoActionDefinedException extends RuntimeException{
	
	private static final long serialVersionUID = 7639522465040911052L;

	public NoActionDefinedException(String msg){
		super(msg);
	}
}
