package org.zhc.exception;

public class NoMethodDefinedException extends RuntimeException {

	private static final long serialVersionUID = 1850786235014486867L;

	public NoMethodDefinedException(String msg) {
		super(msg);
	}

	public NoMethodDefinedException(Exception e) {
		super(e);
	}
}
