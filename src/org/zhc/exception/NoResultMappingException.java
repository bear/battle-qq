package org.zhc.exception;

public class NoResultMappingException extends RuntimeException {

	private static final long serialVersionUID = -1661681929419267880L;

	public NoResultMappingException(Exception e) {
		super(e);
	}

	public NoResultMappingException(String msg) {
		super(msg);
	}
}
