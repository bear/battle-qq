package org.zhc.exception;

public class NoMatchingTypeException extends RuntimeException {

	private static final long serialVersionUID = -7907667160556420595L;

	public NoMatchingTypeException(Exception e) {
		super(e);
	}

	public NoMatchingTypeException(String msg) {
		super(msg);
	}

}
