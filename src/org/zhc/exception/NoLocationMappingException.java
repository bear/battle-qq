package org.zhc.exception;

public class NoLocationMappingException extends RuntimeException {

	private static final long serialVersionUID = -3659542270735697756L;

	public NoLocationMappingException(Exception e) {
		super(e);
	}

	public NoLocationMappingException(String msg) {
		super(msg);
	}

}
