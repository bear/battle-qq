package org.zhc.annotation;

public enum ResponseType {

	FORWARD, INCLUDE, REDIRECT, JSONWRITE, GENERALWRITE;
	public String getStringType(){
		return this.toString().toLowerCase();
	}
}
