package org.zhc.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Result {

	public abstract String value();

	public abstract String location();

	public abstract ResponseType type() default ResponseType.FORWARD;

	public abstract Property[] property() default {};

	public abstract String[] attribute() default {};
}
