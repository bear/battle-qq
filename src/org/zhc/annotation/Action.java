package org.zhc.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Action {

	public abstract String name();

	public abstract String namespace() default "/";

	public abstract String[] param() default {};

	public abstract String[] attribute() default {};
}
