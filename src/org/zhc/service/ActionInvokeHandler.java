package org.zhc.service;

import java.io.IOException;
import java.lang.reflect.Method;
import javax.servlet.ServletException;

import org.zhc.exception.NoActionDefinedException;
import org.zhc.exception.NoLocationMappingException;
import org.zhc.exception.NoMethodDefinedException;
import org.zhc.exception.NoResultMappingException;
import org.zhc.po.Action;
import org.zhc.po.Result;


public interface ActionInvokeHandler {
	public void invoke(String namespace,String actionname)
			throws NoActionDefinedException, ServletException, IOException;

	public Method getMethod(Class<?> clazz, String methodName)
			throws NoMethodDefinedException;

	public Result getResult(String value, Action action)
			throws NoResultMappingException, NoLocationMappingException;

	public void handlerResult(Result result) throws ServletException,
			IOException;

	public void forward(Result result) throws ServletException, IOException;

	public void include(Result result) throws ServletException, IOException;

	public void redirect(Result result) throws IOException;

	public void jsonWrite(Result result) throws IOException;

	public void generalWrite(Result result) throws IOException;

	public void destoryHttpContext();

	public void getParameters();

	public void getFields(Action action);

	public void setFields();

	public void setAttributes(Result result);

	public String rebuildLocation(Result result);
}
