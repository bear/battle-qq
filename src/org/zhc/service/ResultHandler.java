package org.zhc.service;

import java.io.IOException;

import javax.servlet.ServletException;

/**
 * 对调用action完后结果处理的接口类
 * 
 * @author 张浩春
 * 
 */
public interface ResultHandler {

	public void forward() throws ServletException, IOException;

	public void include() throws ServletException, IOException;

	public void redirect() throws IOException;

	public void jsonWrite() throws IOException;

	public void generalWrite() throws IOException;
}
