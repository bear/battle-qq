package org.zhc.service;

import java.io.InputStream;
import org.dom4j.DocumentException;

public interface LoadConfigService {

	public void loadAcionConfig(InputStream xmlInputStream)
			throws DocumentException;

}
