import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.hibernate.Session;
import org.junit.Test;
import org.zhc.util.TypeConvertUtil;

import websocket.constant.Config;
import websocket.dao.HibernateSessionFactory;
import websocket.dao.UsersDAO;
import websocket.pojo.Group;
import websocket.pojo.Users;
import websocket.service.HistoryMsgService;
import websocket.service.impl.HistoryMsgServiceImpl;
import websocket.service.impl.UserService;

public class UnitTest {

	public static void main(String[] args) {
		Session session = HibernateSessionFactory.getSession();
		Group g = (Group) session.load(Group.class, 1);
		System.out.println("--------------" + g.getGroupname());
		Session session2 = HibernateSessionFactory.getSession();
		Group g2 = (Group) session.load(Group.class, 1);
		System.out.println("--------------" + g2.getGroupname());
	}

	@Test
	public void testMap() {
		Long a = 1L;
		Long b = 1L;
		String str = "hello";
		Map<Long, String> map = new HashMap<Long, String>();
		map.put(a, str);
		System.out.println(map.get(b));
	}

	@Test
	public void testGetHistory() {
		HistoryMsgService service = new HistoryMsgServiceImpl();
		java.sql.Date begin = java.sql.Date.valueOf("2011-01-01");
		String his = service.getHistory(1234L, 666L, begin);
		System.out.println(his);
		his = service.getHistory(1234L, 666L, begin);
		System.out.println(his);
	}

	@Test
	public void testCalendar() {
		Calendar c = Calendar.getInstance();
		c.set(Calendar.DAY_OF_WEEK, -3);// new
										// java.sql.Date(c.getTime().getTime()).toString()
		System.out.println(c.getTime().toString());
	}

	@Test
	public void testTime() {
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DAY_OF_MONTH, -2);
		System.out.println(new java.sql.Date(c.getTime().getTime()).toString());
	}

	@Test
	public void testEHCache() {
		UsersDAO dao = new UsersDAO();
		List list1 = dao.getGroups(1234L);
		System.out.println("--------------" + list1.size());
		List list2 = dao.getGroups(1234L);
		System.out.println("--------------" + list2.size());
	}

	@Test
	public void testConvert() {
		String val = null;
		java.sql.Timestamp t = TypeConvertUtil.convert(val,
				java.sql.Timestamp.class);
		System.out.println(t.toString());
	}

	@Test
	public void testUniqueId() {
		UserService service = new UserService();
		for (int i = 0; i < 30; i++) {
			System.out.println(service.productUniqueNum());
		}
	}

	@Test
	public void testInsert() {
		UserService service = new UserService();
		Users user = new Users();
		user.setNickname("测试");
		user.setSex("男");
		user.setOnline(1);
		user.setIcon("1");
		user.setUid(service.productUniqueNum());
		System.out.println(service.insert(user));
	}

	@Test
	public void testCreateGroup() {
		//Group group = new Group(4127L, Config.DEF_GROUP);
		//UsersDAO usersDAO = new UsersDAO();
		//usersDAO.persistence(group);
		UserService service = new UserService();
		service.createDefGroup(4137L);
	}
	
	@Test
	public void testSearch(){
		UserService service = new UserService();
		List<Users> list = service.searchFriendAccurate("张浩春",1);
		for(Users u:list){
			System.out.println(u.getUid()+":"+u.getNickname());
		}
	}
}
