<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>注册结果页面</title>
<style>
body {
	font-family: "微软雅黑"
}
</style>
</head>
<body>
	<div style="text-align: center;" id="mess">
		<c:choose>
			<c:when test="${param.uid!=null}">
				<img src="/img/tool/regisSucc.png" />注册成功啦！您的Bettle号为：<font
					color="red">${param.uid}</font>
			</c:when>
			<c:otherwise>
				<img src="/img/tool/regisFailed.png" />糟糕！注册失败了，<a
					href="/regis.html">重新返回注册吧！</a>
			</c:otherwise>
		</c:choose>
	</div>
	<script type="text/javascript">
		document.getElementById("mess").style.marginTop = (document.body.scrollHeight - 32)
				/ 2 + "px";
	</script>
</body>
</html>