/**
 * 存储聊天窗口的相关信息的bean类
 */
Ext.define("ChatWin",{
	extend:"Ext.data.Model",
	constructor:function(cuid,cwid,title,x,y,width,height,cw){
		this.cuid = cuid;
		this.cwid = cwid;
		this.x = x;
		this.y = y;
		this.width = width;
		this.height =  height;
		this.cw = cw;
		return this;
	},
	fields:[
		{name:"cuid",type:"int"},
		{name:"cwid",type:"string"},
		{name:"title",type:"string"},
		{name:"x",type:"int"},
		{name:"y",type:"int"},
		{name:"width",type:"int"},
		{name:"height",type:"int"},
		{name:"cw",type:"QQWindow"}
	]
});