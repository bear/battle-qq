/**
 * 模拟桌面应用程序的扩展
 */
Ext.define("Ext.ux.Desktop",{
	extend:"Ext.panel.Panel",
	layout:"absolute",
	apps:[],
	listeners:{
		contextmenu:{
			element:"el",
			fn:function(e){
				e.stopEvent();
			}
		}
	},
	initComponent:function(config){
		this.id="desktop";
		this.width = Ext.getDoc().getViewSize().width;
		this.height = Ext.getDoc().getViewSize().height;
		this.taskBar = this.createTaskBar();
		this.items=[this.taskBar];
		this.renderTo=Ext.getBody();
		this.html = "<img src='img/bg/cloud.jpg' id='desktop-theme' style='width:100%;height:100%;'/>";
		this.addListener("afterrender",this.initApp);
		Ext.applyIf(this,config);
		this.callParent();
	},
	/**
	 * 创建任务栏
	 */
	createTaskBar:function(){
		var desk = this;
		return new Ext.ux.TaskBar({
			x:0,
			y:desk.height-37
		});
	},
	/**
	 * 返回桌面中的taskBar
	 */
	getTaskBar:function(){
		return this.taskBar;
	},
	/**
	 * 设定皮肤（背景）
	 */
	setThemes:function(src){
		document.getElementById("desktop-theme").src=src;
	},
	/**
	 * 初始化桌面应用图标
	 */
	initApp:function(){
		var apps = [{icon:"img/app/Bettle.png",text:"Bettle IM",handler:App.imAppHandler},{icon:"img/app/poker.png",text:"斗地主",handler:null}];
		var x = 10,y = 10;
		for(var i=0;i<apps.length;i++){
			if((y+10+80)>=Ext.getDoc().getViewSize().height-37-80){//防止应用过多而定位错误
				x=x+80;
				y=10;
			}
			y = y+i*80;
			var app = new Ext.ux.AppLink({
				x:x,
				y:y,
				icon:apps[i].icon,
				text:apps[i].text,
				handler:apps[i].handler
			});
			this.apps.push(app);
			this.add(app);
		}
		this.addListener("resize",function(cmp,width,height){
			this.taskBar.setWidth(width);
			this.taskBar.setPosition(0,height-37);
		});
	},
	/**
	 * 检查一个应用的快捷方式是否与桌面已经存在的重叠，重叠返回true
	 * @param {String} 需要检测应用的ID
	 * @param {int} x 需要检测应用的X坐标
	 * @param {int} y 需要检测应用的Y坐标
	 * 
	 */
	checkOverlap:function(id,x,y){
		for(var i=0;i<this.apps.length;i++){
			if(id==this.apps[i].id){//如果为本身就跳过
				continue;
			}
			if(x==this.apps[i].getPosition()[0]&&y==this.apps[i].getPosition()[1]){
				return true;
			}
		}
		return false;
	}
});

/**
 * 定义自己的任务栏
 */
Ext.define("Ext.ux.TaskBar",{
	extend:"Ext.toolbar.Toolbar",
	layout:"absolute",
	tasks:[],
	trays:[],
	listeners:{
		contextmenu:{
			element:"el",
			fn:function(e){
				e.stopEvent();
			}
		}
	},
	initComponent:function(config){
		this.width = Ext.getDoc().getViewSize().width;
		this.height = 37;
		this.windowBar = this.createWindowBar();
		this.trayBar = this.createTrayBar();
		this.items=[this.windowBar,this.trayBar];
		this.addListener("afterrender",function(){this.addListener("resize",this.resize);});
		Ext.applyIf(this,config);
		this.callParent();
		this.style={
				background: "url(../img/winbar-bg.gif) repeat-x",
				borderColor: "transparent"
		}
	},
	createWindowBar:function(){
		var bar = this;
		return new Ext.toolbar.Toolbar({
			x:0,
			y:0,
			style:{
				background: "transparent none",
				borderColor: "transparent"
			},
			layout: { overflowHandler: 'Scroller' },
			width:bar.width-200,
			height:37
		});
	},
	createTrayBar:function(){
		var bar = this;
		return new Ext.ux.TrayBar({
			x:bar.getWindowBar().width,
			y:0,
			width:200,
			style:{
				background: "transparent none",
				borderColor: "transparent"
			}
		});
	},
	/**
	 *添加一个任务到任务栏
	 *@param {Ext.Compoment} cmp 窗体
	 *@param {String} icon 窗体的图标路径
	 *@param {String} text 窗体的标题 
	 */
	addWindowBar:function(cmp,icon,text){
		if(this.checkExist(cmp.id)){
			return;
		}
		var desk = this;
		var id = new Date().getTime()+"-bar";
		this.tasks.push({cmpId:cmp.id,barId:id});
		this.windowBar.add({
			id:id,
			height:25,
			width:140,
			enableToggle: true,
            toggleGroup: 'all',
			text:"<div style='text-align:left'>"+text.ellipsis(16)+"</div>",
			icon:icon,
			style:{
				background:"-webkit-linear-gradient(#fff,#8EDADF)",
			},
			listeners:{
				contextmenu:{
					element:"el",
					fn:function(e,t){
						var bar = this;
						var menu = new Ext.menu.Menu({
							x:e.getPoint().x,
							y:e.getPoint().y-80,
							items:[
								{
									text:"最大化",
									handler:function(){
									
									}
								},
								{
									text:"最小化",
									handler:function(){
										
									}
								},
								{
									text:"关闭窗口",
									handler:function(e){
										if(cmp!=undefined&&typeof(cmp)=="object"){
											cmp.destroy();//执行窗体的销毁函数
										}
										desk.removeWindowBar(bar.id);
									}
								}
							],
							listeners:{
								hide:function(){//每次当菜单隐藏时就将其销毁，避免在页面上产生过多的菜单而导致内存溢出
									this.destroy();
								}
							}
						}).show();
					}
				},
				click:{
					element:"el",
					fn:function(e){
						if(cmp.isHidden()){
							cmp.show();
						}else{
							cmp.hide();
						}
					}
				}
			}
		});
	},
	/**
	 * 从当前任务栏中移除一个任务
	 * @param {String} id 任务的ID
	 */
	removeWindowBar:function(id){
		var data = this.findWindowBarId(id);
		if(data!=null){
			this.windowBar.remove(data[1]);
			this.tasks.splice(data[0],1);
		}
	},
	/**
	 *添加一个任务到托盘
	 *@param {Ext.Component} cmp 添加者的对象，也可称之为句柄
	 *@param {String} icon 任务的图标的路径，例如：images/app.png
	 *@param {Function} click 单击事件响应函数
	 *@param {Function} dblclick 双击事件响应函数
	 *@param {Function} contextclick 右键单击事件响应函数
	 */
	addTrayBar:function(cmp,icon,click,dblclick,contextclick){
		var data = this.trayBar.addTray(cmp,icon,click,dblclick,contextclick);
		this.trays.push(data);
	},
	removeTrayBar:function(id){
		var data = this.findTray(id);
		if(data!=null){
			this.trayBar.removeTray(data[1].trayId);
			this.trays.splice(data[0],1);
			
		}
	},
	findTray:function(cmpid){
		for(var i=0;i<this.trays.length;i++){
			if(this.trays[i].cmpId==cmpid) return [i,this.trays[i]];
		}
		return null;
	},
	/**
	 *返回任务栏中windowBar
	 */
	getWindowBar:function(){
		return this.windowBar;
	},
	/**
	 *返回任务栏中trayBar
	 */
	getTrayBar:function(){
		return this.trayBar;
	},
	resize:function(cmp,w,h){
		this.windowBar.setSize(w-200,37);
		this.trayBar.setPosition(w-200,0);
	},
	checkExist:function(id){
		for(var i=0;i<this.tasks.length;i++){
			if(this.tasks[i].cmpId==id){
				return true;
			}
		}
		return false;
	},
	findWindowBarId:function(cmpid){
		for(var i=0;i<this.tasks.length;i++){
			if(this.tasks[i].cmpId==cmpid){
				return [i,this.tasks[i].barId];
			}
		}
		return null;
	}
});

/**
 *自定义托盘扩展，因EXT的组件中没有允许使用hbox并且从右至左排列的，因此编写自己的插件
 */
Ext.define("Ext.ux.TrayBar",{
	extend:"Ext.toolbar.Toolbar",
	ox:0,
	height:37,
	layout:"absolute",
	initComponent:function(config){
		this.ox = this.width;
		var me = this;
		this.addListener("afterrender",function(){
			me.initTimeItem();
		});
		Ext.applyIf(this,config);
		this.callParent();
	},
	/**
	 *添加一个任务到托盘TrayBar中去
	 *
	 *@param {Ext.Component} cmp 添加者的对象，也可称之为句柄
	 *@param {String} icon 任务的图标的路径，例如：images/app.png
	 *@param {Function} click 单击事件响应函数
	 *@param {Function} dblclick 双击事件响应函数
	 *@param {Function} contextclick 右键单击事件响应函数
	 */
	addTray:function(cmp,icon,click,dblclick,contextclick){
		var bar = this;
		bar.ox = bar.ox-26-5;
		if(click==undefined){
			click=function(){}
		}
		if(dblclick==undefined||dblclick==null){
			dblclick = function(){
				cmp.show();
			}
		}
		var bt = new Ext.button.Button({
			x:bar.ox,
			y:5,
			width:26,
			height:26,
			html:"<img src='"+icon+"' style='width:20px;height:20px;' border='0'/>",
			listeners:{
				click:{
					element:"el",
					fn:click
				},
				dblclick:{
					element:"el",
					fn:dblclick
				},
				contextmenu:{
					element:"el",
					fn:contextclick
				}
			}
		});
		this.add(bt);
		return {trayId:bt.id,cmpId:cmp.id};
	},
	/**
	 * 从托盘中移除一个任务
	 * @param {String} id 移除任务的ID
	 */
	removeTray:function(id){
		this.remove(id);
		this.ox+=26+5;
	},
	/**
	 *在TrayBar初始化后，将时间item添加到trayBar上面去
	 *
	 */
	initTimeItem:function(){
		var bar = this;
		bar.ox = bar.ox-40-5;
		var timeItem = new Ext.toolbar.TextItem({
			x:bar.ox,
			y:8,
			width:40,
			text:Ext.Date.format(new Date(), 'G:i'),
			listeners:{
				afterrender:function(cmp,opts){
					Ext.TaskManager.start({//每隔一秒更新一次时间
						run:function(){cmp.update(Ext.Date.format(new Date(), 'G:i'))},
						interval: 1000
					});
				}
			}
		});
		this.add(timeItem);
	}
});

/**
 * 定义桌面快捷方式的扩展
 */
Ext.define("Ext.ux.AppLink",{
	extend:"Ext.container.Container",
	constrain:true,
	floating:true,
	autoShow:true,
	layout:"absolute",
	listeners:{
		contextmenu:{
			element:"el",
			fn:function(e){
				e.stopEvent();
			}
		}
	},
	initComponent:function(config){
		this.width = 60;
		this.height = 65;
		this.items=[this.createAppIcon(),this.createAppText()];
		this.draggable={
  			delegate: this.id,
  			listeners:{
  				dragend:this.fixPosition
  			}
  		}
		this.addListener("move",this.checkMove);
		Ext.applyIf(this,config);
		this.callParent();
	},
	createAppIcon:function(){
		var pid = this.id;
		var link = this;
		return new Ext.form.Label({
			x:10,
			y:5,
			width:40,
			height:40,
			html:"<img src='"+this.icon+"' style='width:40px;height:40px;'/>",
			style:{
				cursor:"pointer"
			},
			listeners:{
				mouseover:{
					element:"el",
					fn:function(e){
						document.getElementById(pid).style.borderRadius="5px";
						document.getElementById(pid).style.border="2px solid #B4C6E0";
						document.getElementById(pid).style.background="gray";//#DDF7FC
						//document.getElementById(pid).style.opacity=0.5;//透明
					}
				},
				mouseout:{
					element:"el",
					fn:function(e){
						document.getElementById(pid).style.border="none";
						document.getElementById(pid).style.background="transparent";
					}
				},
				dblclick:{
					element:"el",
					fn:link.handler
				}
			}
		});
	},
	createAppText:function(){
		return new Ext.form.Label({
			x:0,
			y:45,
			width:60,
			html:this.text.ellipsis(9),
			style:{
				cursor:"pointer",
				textAlign:"center",
				color:"white"
			}
		});
	},
	checkMove:function(cmp,x,y){
		/*if(y>Ext.getDoc().getViewSize().height-37-this.height){
			this.setPosition(x,Ext.getDoc().getViewSize().height-37-this.height);
		}*/
	},
	/**
	 * 拖动结束时的监听函数，负责修正一个APP的位置，这种修正方法是将窗体划分成网格
	 * @param {Object} obj 对象
	 * @param {Ext.EventObject} e 事件对象
	 * @param {Object} opts 可选参数
	 */
	fixPosition:function(obj,e,opts){
		var cmpId = obj.handle.id;
		var x = e.getPoint().x;
		var y = e.getPoint().y;
		x = 80*parseInt(x/80)+10;
		y = 80*parseInt(y/80)+10;
		if(y>=Ext.getDoc().getViewSize().height-37-80){
			y=y-80;
		}
		while(Ext.getCmp("desktop").checkOverlap(cmpId,x,y)){
			if((y+80+10)>=Ext.getDoc().getViewSize().height-37-80){
				y=10;
				x=parseInt(x/80)*80+80+10;
			}
			y=parseInt(y/80)*80+80+10;
		}
		Ext.getCmp(cmpId).setPosition(x,y);
	}
});
