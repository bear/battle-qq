Ext.define("AppContext",{
	statics:{
		screenWidth:Ext.getDoc().getViewSize().width,
		screenHeight:Ext.getDoc().getViewSize().height,
	    desktopWidth:Ext.getDoc().getViewSize().width,
	    desktopHeight:Ext.getDoc().getViewSize().height-35,
	    listWinWidth:300,
	    listWinHeight:500,
	    listWinX:Ext.getDoc().getViewSize().width*0.75,
	    listWinY:50,
	    listWin_isMax:false,
	    cwMap:new Array(),
	    uid:null,
	    cwIsCreate:function(cuid){
	    	if(this.findIndex(cuid)!=null){
	    		return true;
	    	}
	    	return false;
	    },
	    addChatWin:function(cw){
	    	if(this.findIndex(cw.cuid)==null){//需要添加的数据不存在
	    		this.cwMap[this.cwMap.length] = cw;
	    	}
	    },
	    deleteChatWin:function(cuid){//从数组中删除一个对象
	    	this.cwMap.splice(this.findIndex(cuid),1);
	    },
	    findIndex:function(cuid){//根据cuid来获得它在数组中的下标
	    	for(var i=0;i<this.cwMap.length;i++){
	    		if(this.cwMap[i]["cuid"]==cuid){
	    			return i;
	    		}
	    	}
	    	return null;
	    },
	    setUID:function(uid){
	    	this.uid = uid;
	    },
	    getUID:function(){
	    	return this.uid;
	    },
	    setICON:function(icon){
	    	this.icon = icon;
	    },
	    getICON:function(){
	    	return this.icon;
	    },
	    setNickname:function(name){
	    	this.nickname = name;
	    },
	    getNickname:function(){
	    	return this.nickname;
	    },
	    setStatus:function(status){
	    	this.status = status;
	    },
	    getStatus:function(){
	    	return this.status;
	    }
    }
});