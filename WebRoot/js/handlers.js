
var selectIMGID = "null";//当前选中的主题图片的ID

function length(str){
	
}

/**
 * 显示消息提示的循环函数
 * @param s
 */
function noticeMsg(s){
	if(Ext.getCmp("MSGWINDOW")!=null){
		if(Ext.getCmp("MSGWINDOW").msgcount>0){
			if(s){
				document.getElementById("listWindow_tray").style.background="url(./img/icon/msg.png)";
			}else{
				document.getElementById("listWindow_tray").style.background="url(./img/icon/beetle.png)";
			}
			setTimeout("noticeMsg("+(!s)+")",500);
		}else{
			document.getElementById("listWindow_tray").style.background="url(./img/icon/beetle.png)";
		}
	}
}

/**
 *为一个EXT的组件添加事件，因组件事件需要在被渲染后才可以添加
 *
 *cmp EXT组件
 *event 事件名称，比如："click"
 *handler 响应事件的执行函数
 *
 */
function addEvent(id,event,handler){
	if(typeof(handler)=="function"){
		Ext.getCmp(id).addListener("afterrender",function(){
			Ext.get(id).on("click",function(e){
				var fnBody = handler+"";
				fnBody = fnBody.substring(fnBody.indexOf("{")+1,fnBody.lastIndexOf("}"));
				eval(fnBody);//执行函数体部分内容
			});
		});
	}
}

/**
 * 抖动聊天窗口
 * @param wid  窗口ID
 * @param x 窗口原坐标
 * @param y 窗口原坐标
 * @param i 控制抖劝时间的参数
 */
function shake(wid,i,x,y){
	var win = Ext.getCmp(wid);
	if(x==null||x=="undefined"){
		 x = win.getPosition()[0];
		 y = win.getPosition()[1];
	}
	var ey = y<10?y:10;//Y的偏移量
	var ex = x<10?x:10;//X的偏移量
	switch(i%4){
		case 0:
			win.setPosition(x,y-ey);
			break;
		case 1:
			win.setPosition(x+ex,y);
			break;
		case 2:
			win.setPosition(x,y+ey);
			break;
		case 3:
			win.setPosition(x-ex,y);
			break;
		default:
			break;
	}
	if(i<=30){
		i=i+1;
		setTimeout("shake('"+wid+"',"+i+","+x+","+y+")",50);
	}
}

/**
 * 当点击主题时，将该主题替换为桌面背景
 * 
 * @param src
 *            主题图片的SRC
 * @param elID
 *            主题图片的ID
 */
function changeDesktopBg(src, elID) {
	if (selectIMGID != "null") {
		Ext.get(selectIMGID).setStyle("background-color", "#ffffff");
	}
	Ext.get(elID).setStyle("background-color", "#ffffff");
	selectIMGID = elID;
	Ext.getCmp("desktop").setThemes("img/bg/"+src.substring(6));
}


/**
 *好友列表移动的监听事件
 *
 *cmp 被移动的组件
 *width 移动后的宽度width
 *height 移动后的高度height
 *opts 参数
 */
function listWindowResizeListener(cmp,width,height,opts){
	if(!AppContext.listWin_isMax){
		AppContext.listWinWidth = width;
		AppContext.listWinHeight = height;
	}
	Ext.getCmp("friendsPanel").setSize(width,height-90-50);//Ext.getCmp("friendsPanel").getHeight()*height/AppContext.listWinHeight
	Ext.getCmp("listWindow_footer").setWidth(width);
	Ext.getCmp("listWindow_footer").setPosition(0,height-Ext.getCmp("listWindow_footer").getHeight());
}

/**
 *好友列表移动的监听事件
 *
 *cmp 被移动的组件
 *x 移动后的坐标X
 *y 移动后的坐标Y
 *opts 参数
 */
function listWindowMoveListener(cmp,x,y,opts){
	if(!AppContext.listWin_isMax){
		AppContext.listWinX = x;
		AppContext.listWinY = y;
	}
}

//选中某一个表情
function selectFace(src){
	var id = Ext.getCmp("common_face_win").title;
	var wwid = id.substring(0,id.indexOf("_ft"));
	Ext.getCmp("common_face_win").hide();//隐藏表情窗体
	var mess = "<img src='"+src+"'/>";
	Ext.getCmp(wwid).setValue(Ext.getCmp(wwid).getValue()+mess);//更新消息
	Ext.getCmp(wwid).focus();	//TO DO 选中表情后请焦点在输入框中，但目前有问题，焦点位置不对
}

Ext.define("LoginWinHandlers",{
	statics:{
		login:function(){
			var username = Ext.getCmp("uid").getValue();
			var password = Ext.getCmp("password").getValue();
			if (username == null || username.length == 0) {
				Ext.getCmp("noticeLabel").setText("<font color='red'>\u8bf7\u586b\u5199UID</font>", false);
				return;
			}
			if (password == null || password.length == 0) {
				Ext.getCmp("noticeLabel").setText("<font color='red'>\u8bf7\u586b\u5199\u767b\u5f55\u5bc6\u7801</font>", false);
				return;
			}
			AppContext.setStatus(UserStatus.transformStatus(Ext.getCmp("LoginWinStatusLabel").status));
			Ext.getCmp("loginWindow").hide();
			new DesktopService().showListWindow();
			Ext.getCmp("listWindow").addListener("resize",listWindowResizeListener);
			Ext.getCmp("listWindow").addListener("move",listWindowMoveListener);
			Ext.Ajax.request({
					url:"servlet/Login.do",
					method:"post",
					params:{uid:username,password:password},
					timeout:5000,
					success:function(response,opts){
						try{
							var res = parseInt(response.responseText);
							if(res==0) {
								Ext.getCmp("loginWindow").show();
								Ext.getCmp("listWindow").destroy();
								Ext.Msg.alert("错误","用户不存在或密码错误!");
							}else{
								AppContext.setUID(username);
							    WebsocketService.connect();
							}
						}catch(msg){
							Ext.Msg.alert("错误","服务返回数据错误!");
						}
					},
					failure:function(response,opts){
						Ext.getCmp("loginWindow").show();
						Ext.getCmp("listWindow").destroy();
						Ext.Msg.alert("错误","连接服务器超时!请重试");
					}
			});
		},
		regis:function(){
			new DesktopService().openBrowser("/regis.html","用户注册",590,500,false,false,false,false);
		},
		/**
		 * 记录密码的响应事件
		 */
		forgetPassword:function(){
			//ListWindowHandler.searchFriend();
			//TODO 为测试
			MsgWindow.prompt(200,200,"新增分组","请输入分组名称",function(text,input){
			  	if(text=="ok"){
			  		alert(input.getValue());
			  	}
		   });
		},
		/**
		 * 状态选择下拉框的click监听函数
		 */
		statusHandler:function(e,t){
			var laid = Ext.get(e.getTarget().id).parent("label").id;
			var x = Ext.getCmp(laid).getPosition()[0];
			var y = Ext.getCmp(laid).getPosition()[1]+20;
			var combo = Ext.getCmp("LoginWindowStatusCombo");
			if(combo==undefined){
				combo = CMPFactory.createStatusCombo("LoginWindowStatusCombo",LoginWinHandlers.statusComboboxHandler);
			}
			combo.setPosition(x,y);
			combo.show();
		},
		/**
		 * 下拉框中每个选项的监听事件
		 */
		statusComboboxHandler:function(item,e,opts){
			Ext.getCmp("LoginWinStatusLabel").updateStatus(item.itemId);
			AppContext.setStatus(UserStatus.transformStatus(item.itemId));
		}
	}
});


Ext.define("CWBtHandlers",{//聊天窗口的按钮的响应事件
	statics:{
		/**
		 * 发送按钮的响应函数
		 */
		send:function(e,eid){
			var id =(e==null||e==undefined)?eid:e.id;//标准响应函数只应该有一个参数，但为了省代码，ctrl+enter响应事件共用了send方法
			var fid = id.substring(id.indexOf("_")+1,id.lastIndexOf("_"));//好友ID
			var wwid = id.substring(0,id.lastIndexOf("_"))+"_ww";//  发送消息textarea的ID
			var rwid = id.substring(0,id.lastIndexOf("_"))+"_rw";// 接收消息textarea的ID
			var mess = Ext.getCmp(wwid).getValue();
			if(mess!=null&&mess.isEmpty()){
				var x = Ext.getCmp(wwid).getPosition()[0];
				var y = Ext.getCmp(wwid).getPosition()[1];
				y = y+Ext.getCmp(wwid).getHeight()-30;
				MsgWindow.alert(x+5,y,Ext.getCmp(wwid).getWidth()-10,"不能发送空的消息!");
				return;
			}
			var send = WebsocketService.sendMsg(fid,mess);
			if(!send){
				Ext.getCmp(rwid).setText(document.getElementById(rwid).innerHTML+"<div style='color:red'>对不起，与服务器断开连接或连接异常，无法发送消息!请尝试重新登录</div>",false);
				Ext.get(rwid).scroll("bottom",document.getElementById(rwid).innerHTML.length,false);//滚动至底部
				Ext.get(rwid).scroll("right",document.getElementById(rwid).innerHTML.length,false);//滚动至左部
				return;
			}
			mess = "&nbsp;<span style='color:blue;'>"+AppContext.getNickname()+"("+AppContext.getUID()+") "+Ext.Date.format(new Date(),"Y-m-d G:i:s")+"</span><div style='margin-left:15px;'>"+Ext.getCmp(wwid).getValue()+"</div>";
			Ext.getCmp(wwid).setValue("");
			Ext.getCmp(wwid).focus();
			Ext.getCmp(rwid).setText(document.getElementById(rwid).innerHTML+mess,false);
			Ext.get(rwid).scroll("bottom",document.getElementById(rwid).innerHTML.length,false);//滚动至底部
			Ext.get(rwid).scroll("right",document.getElementById(rwid).innerHTML.length,false);//滚动至左部
		},
		/**
		 * 聊天窗口的关闭按钮的响应函数
		 * @param {Ext.EventObject} e 
		 */
		close:function(e){
			var id = e.id==null?e.getTarget().id:e.id;
			var wid;
			wid = id.substring(0,id.lastIndexOf("_cbt"));
			Ext.getCmp(wid).hide();
		},
		/**
		 * 表情按钮的响应函数
		 */
		faceTool:function(e){//表情图标的响应事件
			var win = Ext.getCmp("common_face_win");
			var x = e.getPosition()[0];
			var y = e.getPosition()[1]
			if(win==undefined){
				var faceId =e.id;
				win = CMPFactory.createFaceToolWin(x,y);
				win.show();
				win.setTitle(faceId);
			}else{
				var faceId =e.id;
				win.setTitle(faceId);
				win.setPosition(x,y-270);
				win.show();
			}
		},
		/**
		 * 清屏的响应函数
		 */
		clearScreen:function(e,t){
			var btid = e.id;
			var rwid;
			if(Ext.getClassName(e)=="Ext.button.Button"){
				var wid = btid.substring(0,btid.lastIndexOf("_ww_clearSc"));//如果为HtmlEditor中的工具按钮
				rwid = wid+"_rw";
			}else{//如果有右键菜单
				console.log("menu:"+e.id);
				rwid = btid.substring(0,btid.lastIndexOf("_clearMenu"));
			}
			Ext.getCmp(rwid).setText("");
		},
		/**在接收信息的界面上的右键清空内容的响应事件
		 * 
		 * @param {Ext.EventObject} e  右键的event对象
		 * @param {HtmlElement} t 事件的发起者
		 * @param {Object} o 未知
		 * @param {Object} options 可选参数
		 */
		clearScreenMenu:function(e,t,o,opts){
			e.stopEvent();
			var cla = this;
			var menu = Ext.getCmp("chatWinReviceMenu");
			var id = t.id;
			if(id.indexOf("ext-gen")==0){
				id = Ext.get(t.id).parent("label").id;
			}
			if(menu==undefined){
				menu = new Ext.menu.Menu({
					id:"chatWinReviceMenu",
					items:[
					       {
					    	   id:id+"_clearMenu",
					    	   iconCls:"icon-clearScreeen",
					    	   text:"清空屏幕",
					    	   handler:CWBtHandlers.clearScreen
					       }
					],
					x:e.getPoint().x,
					y:e.getPoint().y
				});
				menu.show();
			}else{
				menu.getComponent(0).id=id+"_clearMenu";
				menu.setPosition(e.getPoint().x,e.getPoint().y);
				menu.show();
			}
		},
		/**
		 * 历史记录响应事件
		 */
		historyHandler:function(e){
			var tid = e.id;
			var wid = tid.substring(0,tid.lastIndexOf("_ww_history"));
			var win = Ext.getCmp(wid);
			var body = win.getBody();
			if(Ext.getCmp(wid+"_hisWin")==null){
				var uid = wid.substring(wid.indexOf("_")+1);
				win.setSize(650,win.getHeight());
				var hisLa = CMPFactory.createCommonLabel(wid+"_hisWin",360,30,270,350,"正在加载历史记录...");
				hisLa.setAutoScroll(true);
				body.add(hisLa);
				var service = new MessageService();
				service.loadHistory(hisLa,uid,7);
			}else{
				Ext.getCmp(wid+"_hisWin").destroy();
				win.setSize(500,win.getHeight());
			}
		},
		/**
		 * 抖动按钮的响应函数
		 */
		shakeHandler:function(e){			
			var tid = e.id;
			var wid = tid.substring(0,tid.lastIndexOf("_ww_shake"));
			var uid = wid.substring(wid.indexOf("cw_")+3);
			var x = Ext.getCmp(wid+"_rw").getPosition()[0];
			var y = e.getPosition()[1];
			if(Ext.getCmp("cu_"+uid).online==0){
				MsgWindow.alert(x,y-28,190,"<div style='margin-top:5px;float:left;'><img src='./img/tool/disabled.png'/></div><div style='flat:left;'>该好友不在线或隐身，无法抖动!</div>");
				return;
			}
			if(MessageService.canShake(uid)){
				WebsocketService.sendShakeMsg(uid);
				shake(wid,0);
			}else{
				MsgWindow.alert(x,y-28,120,"<div style='margin-top:5px;float:left;'><img src='./img/tool/disabled.png'/></div><div style='flat:left;'>您抖动的太频繁啦!</div>");
			}
		},
		/**
		 * 将一个任务添加到托盘
		 */
		addToJob:function(cmp,opts){
			var id = cmp.id;
			Ext.getCmp("desktop").getTaskBar().addWindowBar(cmp,cmp.icon,cmp.title);
		},
		/**
		 * 从任务栏中移除任务
		 * @param {QQWindow} cmp 容器
		 * @param {Object} opts 可选参数
		 */
		removeJob:function(cmp,opts){
			var id = cmp.id;
			AppContext.deleteChatWin(id);
			Ext.getCmp("desktop").getTaskBar().removeWindowBar(id);
		},
		/**
		 * 聊天窗口的消息发送框的快捷键监听事件
		 * @param {Ext.form.field.HtmlEditor} editor 
		 * @param {Ext.EventObject} e 事件对象
		 */
		keyPressListener:function(editor,e){
			if(e.getKey()==10){
				CWBtHandlers.send(null,editor.id);
			}
		}
	}
});

/**
 * 应用桌面初始化等基本的处理类
 */
Ext.define("App",{//应用的基本控制类
	jobs:null,
	statics:{
		/**
		 * 初始化桌面的函数
		 */
		initDesktop:function(){
			new Ext.ux.Desktop();
			// 注册右键
			Ext.get("desktop").on("contextmenu", function(e) {
				desMenu.setPosition(e.getPoint().x,e.getPoint().y);
				desMenu.show(); // 显示在当前位置 
			});
			var desktopService = new DesktopService();
			desMenu = desktopService.initDesMenu(); 
		},
		/**
		 * 从任务栏中移除一个任务
		 * @param jobid 任务栏中任务窗体的ID
		 */
		removeJob:function(jobid){
			Ext.getCmp("JOBCON").remove(jobid);
			for(var i=0;i<this.jobs.length;i++){
				if(this.jobs[i]==jobid) this.jobs.splice(i,1);
			}
			AppContext.deleteChatWin(jobid);
		},
		/**
		 *显示桌面的响应函数
		 */
		showDesktopHandler:function(){
			var wins = Ext.getDoc().query("div[class='x-container x-container-default x-layer']");
			for(var i=0;i<wins.length;i++){
				Ext.getCmp(wins[i].id).hide();
			}
		},
		/**
		 * 主题设置的响应函数
		 */
		themeSettingHandler:function(){
			new QQWindow({
				title:"主题设置",
				icon:"img/icon/theme.png",
				width:540,
				height:430, 
				resizable:false,
				bodyItems:[
					{
						xtype:"label",
						x:2,
						y:2,
						width:536,
						height:400,
						html:new DesktopService().initThemesHTML()
					}
				]
			}).show();
		},
		/**
		 * 右键菜单的系统设置响应函数
		 */
		sysSettingHandler:function(e){
			//TODO 系统设置处理
			Ext.Msg.alert("handle", "sys setting");
		},
		/**
		 * 窗口底部的设置响应函数
		 */
		settingHandler:function(e){
			//TODO 设置按钮的处理
		},
		imAppHandler:function(){
			if(Ext.getCmp("loginWindow")==undefined||Ext.getCmp("loginWindow")==null){
				new DesktopService().showLoginWindow();
			}else{
				Ext.getCmp("loginWindow").show();
			}
		}
	}
});

/**
 * 列表窗口相关的处理类
 */
Ext.define("ListWindowHandler",{
	statics:{
		/**
		 * 列表窗口中托盘的单击响应函数
		 */
		click:function(e){
			var mw = Ext.getCmp("MSGWINDOW");
			if(mw!=null){
				if(mw.msgcount>0){
					var win = Ext.getCmp(e.getTarget().id);
					var x = win.getPosition()[0];
					var y = win.getPosition()[1];
					mw.setPosition(x-150,y-(25*mw.msgcount));
					mw.setHeight(25*mw.msgcount);
					mw.show();
				}
			}
		},
		/**
		 * 打开一个新的窗口
		 * @param {string} cuid 用户显示信息容器的ID
		 * @param {string} name 用户名
		 * @param {int}  icon 用户的图像编号
		 */
		openChatWin:function(cuid,name,icon){ 
			if(AppContext.cwIsCreate(cuid)){
				AppContext.getChatWin(cuid).cw.show();
			}else{
				var title = "与["+name+"]聊天中";
				var win = CMPFactory.createChatWin("cw_"+cuid,(AppContext.desktopWidth-500)/2,(AppContext.desktopHeight-450)/2,500,450,icon,title);//[null,null,CWBtHandlers.closeHandler]
				var cw = new ChatWin(cuid,"cw_"+cuid,title,(AppContext.desktopWidth-500)/2,(AppContext.desktopHeight-450)/2,500,450,win);
				AppContext.addChatWin(cw);
				win.show();
				Ext.getCmp("cw_"+cuid+"_ww").focus();
			}
		},
		/**
		 * 好友列表窗口关闭按钮的处理函数
		 */
		close:function(e){
			var btid = e.getTarget().id;
			var wid = btid.substring(0,btid.indexOf("_close_bt"));
			Ext.Msg.confirm("提示","确定退出聊天?",function(btn){
				if(btn=="yes"){
					WebsocketService.sendOfflineMsg();//发送下线消息后将socket关闭
					WebsocketService.offline = 302;
					WebsocketService.socket.close();
					WebsocketService.socket=null;
					Ext.getCmp(wid).destroy();//销毁列表窗口
					while(AppContext.cwMap.length>0){//销毁所有聊天窗口 
						AppContext.cwMap[0].cw.destroy();						
					}
				}
			});
		},
		/**
		 * 状态选择下拉框的click监听函数
		 */
		statusHandler:function(e,t){
			var laid = Ext.get(e.getTarget().id).parent("label").id;
			var x = Ext.getCmp(laid).getPosition()[0];
			var y = Ext.getCmp(laid).getPosition()[1]+20;
			var combo = Ext.getCmp("ListWindowStatusCombo");
			if(combo==undefined){
				combo = CMPFactory.createStatusCombo("ListWindowStatusCombo",ListWindowHandler.statusComboboxHandler);
			}
			combo.setPosition(x,y);
			combo.show();
		},
		/**
		 * 下拉框中每个选项的监听事件
		 */
		statusComboboxHandler:function(item,e,opts){
			if(AppContext.getStatus()!=UserStatus.transformStatus(item.itemId)){//只有当用户真正改变了状态时才执行
				Ext.getCmp("ListWinStatusLabel").updateStatus(item.itemId);
				AppContext.setStatus(UserStatus.transformStatus(item.itemId));
				WebsocketService.sendChangeStatusMsg(MessageCode.getCode(AppContext.getStatus()),AppContext.getStatus());
			}
		},
		searchFriend:function(){
			//方便测试，固定ID
			//AppContext.setUID(2105084);
			new DesktopService().openBrowser("/searchFriend.jsp?id="+AppContext.getUID(),"查找好友",754,480,false,false,false,false);
		},
		addFriend:function(id){
			var browser = Ext.getCmp(id);
			if(browser!=null){
				browser.setPosition((Ext.getDoc().getViewSize().width-485)/2,(Ext.getDoc().getViewSize().height-331)/2);
				browser.updateTitle("img/tool/browser.png","添加好友");//475 331
				browser.setSize(466,328);
			}
		},
		/**
		 * 关闭一个模拟浏览器
		 */
		closeBrowser:function(id){
			var browser = Ext.getCmp(id);
			if(browser!=null){
				browser.destroy();
			}
		},
		userContextMenu:function(e,t,o,opts){
			var menu = CMPFactory.createUserMenu(opts.gid,opts.id,e.getPoint().x,e.getPoint().y);
			//menu.setPosition(e.getPoint().x,e.getPoint().y);
			menu.show();
		},
		groupContextMenu:function(e,t,o,opts){
			var menu = CMPFactory.createGroupMenu(opts.gid,e.getPoint().x,e.getPoint().y);
			//menu.setPosition(e.getPoint().x,e.getPoint().y);
			menu.show(); 
		},
		createGroup:function(tx,input){
			if(tx=="ok"){
				alert("您需要创建分组的名称是："+input.getValue());
			}
		}
	}
});
