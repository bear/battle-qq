/**
 * 创建组件的工厂类
 */
Ext.define("CMPFactory",{
	statics:{
		createLabel:function(id,text,fontSize,x,y,handler){
			return new Ext.form.Label({
				id:id,
				html:"<div style='font-size:"+fontSize+"px;'>"+text+"</div>",
				x:x,
				y:y,
				listeners:{
					click:{
						element:"el",
						fn:handler
					}
				}
			});
		},
		createCommonLabel:function(id,x,y,width,height,html,contexthandler){
			return new Ext.form.Label({
				id:id,
				x:x,
				y:y,
				width:width,
				height:height,
				html:html,
				style:{
					backgroundColor:"white",
					border:"1px solid #CEE3F6"
				},
				autoScroll:true,
				listeners:{
					contextmenu:{
						element:"el",
						fn:contexthandler
					}
				}
			});
		},
		/**
		 * 创建任务栏中的容器
		 * @param {string} id 任务窗体的ID
		 * @param {int} width 窗体的宽度
		 * @param {int} len 截取标题的长度
		 * @param {string} icon 任务图标 
		 * @param {string} jobName 任务名称
		 */
		createJobLabel:function(id,width,len,icon,jobName){//创建底部的任务图标
			var nJobName = jobName.length>len?jobName.substring(0,len)+"...":jobName;
			var rid = "ext-job-"+new Date().getTime();
			return new Ext.form.Label({
				id:id,
				height:35,
				width:width,
				html:"<div title='"+jobName+"' id="+rid+" style='width:"+width+"px;height:35px;background:-webkit-linear-gradient(#B8CCD0,#A6A6A5);border-radius:3px;'><span style='width:25px;height:25px;margin-top:5px;display:block;float:left;'><img src='"+icon+"'/></span><span style='height:25px;padding-top:5px;line-height:25px;display:block;float:left'>"+nJobName+"</span></div>",
				style:{
					cursor:"pointer"
				},
				listeners:{
					click:{
						element:"el",
						fn:function(e){
							var wid = this.id.substring(0,this.id.lastIndexOf("_job"));
							if(Ext.getCmp(wid).isVisible(true)){
								Ext.getCmp(wid).hide();
							}else{
								Ext.getCmp(wid).show();
							}
						}
					},
					mouseover:{
						element:"el",
						fn:function(){
							document.getElementById(rid).style.background="-webkit-linear-gradient(#fff,#6B959E)"
						}
					},
					mouseout:{
						element:"el",
						fn:function(){
							document.getElementById(rid).style.background="-webkit-linear-gradient(#fff,#A6A6A5)"
						}
					}
				}
			});
		},
		/**
		 * 创建聊天窗口发送消息的HtmlEditor
		 * @param {String} id 编辑器的ID
		 * @param {int} x 坐标X
		 * @param {int} y 坐标Y
		 * @param {int} width 宽度
		 *  @param {int} height 高度
		 *  
		 */
		createHtmlEditor:function(id,x,y,width,height){
			var editor = new Ext.form.field.HtmlEditor({
				id:id,
				x:x,
				y:y,
				width:width,
				height:height,
				enableSourceEdit:false,
				enableAlignments:false,
				enableFont:false,
				enableLinks:false,
				enableLists:false,
				listeners:{
					keypress:CWBtHandlers.keyPressListener,
					afterrender:function(cmp,opts){
						this.getToolbar().add({
								id:id+"_ft",
								iconCls:"icon-face",
								handler:CWBtHandlers.faceTool
							},
							{
								id:id+"_shake",
								iconCls:"icon-shake",
								handler:CWBtHandlers.shakeHandler
							},
							{
								id:id+"_clearSc",
								iconCls:"icon-clearScreeen",
								handler:CWBtHandlers.clearScreen
							},
							{
								id:id+"_history",
								text:"历史记录",
								handler:CWBtHandlers.historyHandler
							}
						);
						this.focus(true);
						Ext.tip.QuickTipManager.init();
						Ext.tip.QuickTipManager.register({
						    target: id+"_ft",
						    text: '表情',
						    dismissDelay: 1000
						});
						Ext.tip.QuickTipManager.register({
						    target: id+"_shake",
						    text: '抖动一下',
						    dismissDelay: 1000
						});
						Ext.tip.QuickTipManager.register({
						    target: id+"_clearSc",
						    text: '清除屏幕',
						    dismissDelay: 1000
						});
					}
				}
			});
			return editor;
		},
		createTextfield:function(id,name,type,x,y,width,height,reg,emptyText){
			return new Ext.form.TextField({
				id:id,
				name:name,
				inputType:type,
				x:x,
				y:y,
				width:width,
				height:height,
				maskRe:reg,
				emptyText:emptyText
			});
		},
		createButton:function(text,id,icon,x,y,width,height,handler){
			return new Ext.Button({
				id:id,
				text:"<span style='font-size:14px;'>"+text+"</span>",
				iconCls:icon,
				x:x,
				y:y,
				width:width,
				height:height,
				handler:handler
			});
		},
		createCheckbox:function(text,x,y,checked){
			return new Ext.form.Checkbox({
				x:x,
				y:y,
				boxLabel:text,
				checked:checked
			});
		},
		createIconTool:function(id,x,y,padding,icon,title,handler){//创建聊天窗口中的工具
			return new Ext.form.Label({
				id:id,
				x:x,
				y:y,
				width:26,
				height:26,
				html:"<div style='width:26px;height:26px;' title='"+title+"'><img src='"+icon+"' style='padding:"+padding+"px;' id='"+id+"_icon'/></div>",
				listeners:{
					click:{
						element:"el",
						fn:handler
					},
					mouseover:{
						element:"el",
						fn:function(e){
							document.getElementById(this.id).style.border="1px solid #0486DE";
						}
					},
					mouseout:{
						element:"el",
						fn:function(e){
							document.getElementById(this.id).style.border="none";
						}
					},
				},
				style:{
					cursor:"pointer"
				}
			});
		},
		createContainer:function(id,x,y,width,height,html,items,layout){//创建一个透明容器
			return Ext.create("Ext.container.Container",{
				id:id,
				x:x,
				y:y,
				width:width,
				height:height,
				html:html,
				items:items,
				layout:layout
			});
		},
		createUserMenu:function(gid,uid,x,y){
			return new Ext.menu.Menu({
				x:x,
				y:y,
				items:[
				   {
					   text:"删除好友",
					   iconCls:"icon-user-delete",
					   handler:function(e){
						   Ext.getCmp("groupChild_"+gid).remove("cu_"+uid);
					   }
				   },{
					   text:"修改备注名称",
					   iconCls:"icon-user-edit"
				   },{
					   text:"把好友移动到",
					   handler:function(){
						   Ext.Msg.alert("handle", "暂未开发");
					   }
				   }
				],
				listeners:{
					hide:function(){
						this.destroy();
					}
				}
			});
		},
		createGroupMenu:function(gid,x,y){
			return new Ext.menu.Menu({
				x:x,
				y:y,
				items:[
				   {
					   text:"新增分组",
					   handler:function(t,e){
						   MsgWindow.prompt(t.getPosition()[0],t.getPosition()[1],"新增分组","请输入分组名称",ListWindowHandler.createGroup);
					   }
				   },{
					   text:"更改分组名称"
				   },{
					   text:"删除分组"
				   },{
					   text:"刷新分组"
				   }
				],
				listeners:{
					hide:function(){
						this.destroy();
					}
				}
			});
		},
		/**
		 * 创建桌面上应用的快捷方式
		 * @param {String} id 指定ID
		 * @param {int} x所在坐标X
		 * @param {int} 所在坐标Y
		 * @param {String} 快捷方式图标
		 * @param {Function} handler 双击的响应函数 
		 */
		createAppICON:function(id,x,y,icon,handler){
			new Ext.form.Label({
				id:id,
				x:x,
				y:y,
				width:50,
				height:50,
				floating:true,
				renderTo:"sysBody",
				style:{
					background:"url("+icon+")",
					cursor:"pointer"
				},
				listeners:{
					click:{
						element:"el",
						fn:handler
					}
				}
			});
		},
		/**
		 * 创建类似下拉框的状态选择
		 * @param id 组件的ID
		 * @param handler 每个item的处理函数
		 */
		createStatusCombo:function(id,handler){
			return new Ext.menu.Menu({
				id:id,
				width:70,
				maxWidth:70,
				items:[
					{
						text:"在线",
						iconCls:"icon-online",
						itemId:"online",
						width:65,
						maxWidth:65,
						handler:handler
					},
					{
						text:"忙碌",
						iconCls:"icon-busy",
						itemId:"busy",
						width:65,
						maxWidth:65,
						handler:handler
					},
					{
						text:"离线",
						iconCls:"icon-away",
						itemId:"away",
						width:65,
						maxWidth:65,
						handler:handler
					},
					{
						text:"隐身",
						itemId:"invisible",
						iconCls:"icon-invisible",
						width:65,
						maxWidth:65,
						handler:handler
					}
				]
			});
		},
		createToolLabel:function(id,x,y,width,height,icon,title){//创建工具label，主要是用于QQ样式窗体的右上角按钮
			return new Ext.form.Label({
				id:id,
				x:x,
				y:y,
				width:width,
				height:height,
				style:{
					background:"url("+icon+")",
					cursor:"pointer"
				}
			});
		},
		createButtonLabel:function(id,x,y,text,iconCls,handler){
			if(id==null||id==""){
				id = "label_"+new Date().getTime();
			}
			var label =  new Ext.form.Label({
				id:id,
				xtype:"label",
				x:x,
				y:y,
				html:"<div class='toolLable'><span style='display:block;width:16px;height:16px;float:left' class='"+iconCls+"'></span>&nbsp;<span style='display:block;float:left;'>&nbsp;"+text+"</span></div>",
				style:{
					cursor:"pointer"
				}
			});
			if(handler!=null&&typeof(handler)=="function"){
				addEvent(id,"click",handler);
			}
			return label;
		},
		/**
		 * 创建聊天窗口
		 * @param {string} cwid 窗体ID
		 * @param {int} x 窗体坐标X
		 * @param {int} y 窗体坐标Y
		 * @param {int} width 窗体的宽度
		 * @param {int} height 窗体的高度
		 * @param {string} icon 窗体的图标
		 * @param {string} title 窗体的标题
		 * @param {Array} handlers 三个按钮的处理函数
		 */
		createChatWin:function(cwid,x,y,width,height,icon,title,handlers){//创建聊天窗口
			items=[
				this.createCommonLabel(cwid+"_rw",0,30,350,240,"",CWBtHandlers.clearScreenMenu),
				this.createHtmlEditor(cwid+"_ww",0,270,350,110),
				this.createContainer(cwid+"_btcon",0,380,350,40,"",[this.createButton("发&nbsp;送(S)",cwid+"_sbt","",350-10-70,8,70,24,CWBtHandlers.send),this.createButton("关&nbsp;闭(C)",cwid+"_cbt","",350-10*2-70*2,8,70,24,CWBtHandlers.close)],"absolute")
			];
			var win = this.createQQStyleWindow(cwid,x,y,width,height,items,icon,title,false,[true,false,true],handlers);
			win.addListener("show",CWBtHandlers.addToJob);
			win.addListener("destroy",CWBtHandlers.removeJob);
			return win;
		},
		/**
		 * 创建表情显示窗体
		 * @param {int} X 窗体显示的坐标X
		 * @param {int} Y 窗体显示的坐标Y
		 */
		createFaceToolWin:function(x,y){
			var html="<div>";
			for(var i=0;i<130;i++){
				if(i>0&&i%13==0) html+"<br/>";
				html+="<img src='./img/face/"+i+".gif' onmouseover='javascript:this.style.border=\"1px solid #0486DE\"' onmouseout='javascript:this.style.border=\"none\"' onclick='selectFace(this.src)' style='padding:1px;width:26px;height:26px;cursor:pointer;'/>";
			}
			html+="</div>";
			return new Ext.window.Window({id:"common_face_win",x:x,y:y-270,width:350,height:270,preventHeader:true,border:0,html:html,bodyBorder:false,resizable:false,style:{backgroudColor:"white"}});
		},
		/**
		 * 创建QQ样式窗体的函数
		 * @param {string} id 窗体ID
		 * @param {int} x 在浏览器中的坐标X
		 * @param {int} y 在浏览器中的坐标Y
		 * @param {int} width 窗体的宽度
		 * @param {int} height 窗体的宽度
		 * @param {Array} items 窗体的子容器的数组
		 * @param {string} icon 窗口的小图标，此图标标准大小为25X25
		 * @param {string} title 窗体上面显示的标题
		 * @param {boolean} resize 是否允许更改窗体大小 
		 * @param {Array} toolAble min max close三个按钮中控制显示哪几个按钮
		 * @param {Array} toolHandlers 三个按钮的处理函数，如果不提供，那么会启用默认处理函数
		 * @return {QQWindow} window
		 */
		createQQStyleWindow:function(id,x,y,width,height,items,icon,title,resize,toolAble,toolHandlers){//创建QQ样式的窗体
			var minHand =null,maxHand=null,closeHand=null;
			try{
				minHand=toolHandlers[0];
			}catch(msg){}
			try{
				maxHand=toolHandlers[1];
			}catch(msg){}
			try{
				closeHand=toolHandlers[2];
			}catch(msg){}
			var window = new QQWindow({
				id:id,
				x:x,
				y:y,
				width:width,
				height:height,
				bodyItems:items,
				icon:icon,
				title:title,
				floating:true,
				resizable:resize,
				minable:Ext.isEmpty(toolAble)?false:toolAble[0],
				maxable:Ext.isEmpty(toolAble)?false:toolAble[1],
				closable:Ext.isEmpty(toolAble)?true:toolAble[2],
				minAction:minHand,
				maxAction:maxHand,
				closeAction:closeHand
			});
			return window;
		}
	}
});
