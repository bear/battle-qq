/**
 * 拓展string的方法，这个方法是用来检测带有&nbsp;的HTML字符串是否为空
 */
String.prototype.isEmpty=function(){
	var str = this+"";
	if(str!=null&&str!=undefined){
		while(str.indexOf("&nbsp;")!=-1){
			str = str.replace("&nbsp;","");
		}
		str = str.trim();
	}
	return str.length==0;
}

String.prototype.realLen=function(){
	var str = this+"";
	var len = 0;
	for(var i=0;i<str.length;i++){
		if(str[i].charCodeAt(0)<299){
			len++;
			continue;
		}
		len+=2;
	}
	return len;
}

/**
 * 当一个字符串长度超过给定的参数时，为截取给定长度并在后面加上...，需要说明的是这个方法针对汉字进行了改进
 */
String.prototype.ellipsis=function(len){
	var str = this+"",temp=0;
	if(str.realLen()<=len){
		return str;
	}
	for(var i=0;i<str.length;i++){
		if(temp>=len){
			return str.substring(0,i)+"...";
		}
		if(str[i].charCodeAt(0)>299){
			temp+=2;
		}else{
			temp++;
		}
	}
	return str.substring(0,len)+"...";
}
