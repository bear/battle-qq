Ext.define("UserStatus",{
	statics:{
		OFFLINE:0,
		ONLINE:1,
		BUSY:2,
		AWAY:3,
		INVISIBLE:4,
		transformStatus:function(strStatus){
			switch(strStatus){
			case "offline":
				return UserStatus.OFFLINE;
			case  "online":
				return UserStatus.ONLINE;
			case "busy":
				return UserStatus.BUSY;
			case "away":
				return UserStatus.AWAY;
			case "invisible":
				return UserStatus.INVISIBLE;
			default:
				return UserStatus.OFFLINE;
			}
		},
		transformStatusToText:function(strStatus){
			switch(strStatus){
			case "offline":
				return "下线";
			case  "online":
				return  "在线";
			case "busy":
				return "忙碌";
			case "away":
				return "离线";
			case "invisible":
				return "隐身";
			default:
				return "下线";
			}
		}
	}
});

/**
 * 表示消息类型常量的类
 */
Ext.define("MessageCode",{
	statics:{
		   USER_ONLINE:111,
		   USER_OFFLINE:112,
		   USER_BUSY:113,
		   USER_AWAY:114,
		   USER_INVISIBLE:115,
		   USER_REONLINE:116,
		   SYS_ONLINE:301,// 用户发给系统表示自己上线
		   SYS_OFFLINE:302,// 用户发给系统表示自己下线
		   SYS_BUSY:303,// 用户发给系统表示自己状态更改为忙碌的消息
		   SYS_AWAY:304,// 用户发给系统表示自己状态更改为离线的消息
		   SYS_INVISIBLE:305,// 用户发给系统表示自己状态更改为隐身的消息
		   SYS_REONLINE:306,// 用户发给系统表示自己状态更改为上线的消息与301状态不同
		   getCode:function(status){
			   switch(status){
			   case UserStatus.ONLINE:
				   return  MessageCode.SYS_REONLINE;
			   case UserStatus.BUSY:
				   return  MessageCode.SYS_BUSY;
			   case UserStatus.AWAY:
				   return  MessageCode.SYS_AWAY;
			   case UserStatus.INVISIBLE:
				   return  MessageCode.SYS_INVISIBLE;
			   default:
				   return -1;
			   }
		   }
	}
});