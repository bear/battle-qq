/*
 树中节点ID标识说明：
 	1、最外面的Main容器的ID是命名方式是：new Date().getTime()_mainContainer,容器ID通常不会使用
 	2、每个子容器（分组容器）的ID命名方式是：groupContainer_id，此ID为这个分组的ID，必须唯一
 	3、每个分组的头部的容器（显示分组名称等）的ID命名方式是：groupHead_id，此ID为这个分组的ID，必须唯一
 	4、每个分组的头部的容器（显示分组名称等）的展开/收缩图标的span的ID命名方式是：groupHead_id_icon，此ID为这个分组的ID，必须唯一
 	5、每个分组的头部的容器（显示分组名称等）的在线/总数统计的span的ID命名方式是：groupHead_id_count，此ID为这个分组的ID，必须唯一
 	6、每个分组的子节点的容器ID的命名方式是：groupChild_id，此ID为这个分组的ID，必须唯一
 	7、每个子节点的容器的命名方式是：cu_id，此ID为这个子节点的ID,必须唯一
 设计说明：
 	1、每个用户的个性图像都有四张：id.jpg、id_h.jpg、id_m.jpg、id_m_h.jpg，这样的话在更改在线状态比较好办，因此从后台传过来的只是一个编号，
 	   实际再根据在线状态生成图像路径
 	2、在ZTreeBase类中有一方法：getGroup()，这个方法是根据分组ID来查询该分组的一些信息，它返回的是一个一维数组，长度为3，其数据信息分别是：
 	   0：这个数据在gf中的下标
 	   1：这个分组当前展开/收缩状态
 	   2：这个分组容器的高度
 	   3:这个分组上次展开时的历史高度
 	3、在ZTreeBase类中，有个非常重要的属性：gf，这个属性是groupFriend缩写，是用来存储每个分组的相关信息，它是一个JSON格式数据，为数组，每个对象有以下几个属性：
 	  1、id 分组ID
 	  2、flag 分组是否展开的标记 0为收缩 1为展开
 	  3、height 分组容器的高度 默认情况下为30，也就是头部的高度，当加载分组数据加载完毕时会自动更改其高度
 	  4、oldHeight 上一次执行收缩/展开时的高度，这个属性主要是为了避免分组数据加载较慢，而用户在收缩或展开分组时出现高度不对的问题
 	  5、load 这个分组是否已经加载完毕，默认为0 0未加载 1已加载完毕
*/
Ext.define("ZTreeBase",{
	extend:"Ext.container.Container",
	gf:[],//用来存储每个组是否展开的标记JSON
	mainHeight:0,
	constructor:function(){
		return this;
	},
	createGroup:function(childUrl,data,mainId,mainX,mainY,mainWidth){
		this.groupWidth = mainWidth;
		this.mainCon=this.createContainer(new Date().getTime()+"_mainContainer",mainX,mainY,mainWidth,this.mainHeight,"vbox");
		if(typeof(data)=="object"&&data.length>0){
			var json = "";
			this.mainHeight = 30*data.length;
			this.mainCon.setHeight(this.mainHeight);
			for(var i=0;i<data.length;i++){//先将所有分组加载完成
				json+=",{\"id\":"+data[i]["id"]+",\"flag\":0,\"height\":30,\"oldHeight\":30,\"load\":0}";
				var groupCon = this.createContainer("groupContainer_"+data[i]["id"],0,0,mainWidth,30,"absolute");
				var groupHead = this.createGroupHead("groupHead_"+data[i]["id"],mainWidth,30,data[i]["name"]);
				var childrenCon = this.createContainer("groupChild_"+data[i]["id"],30,30,mainWidth-30,0,"vbox");
				groupCon.add(groupHead);
				groupCon.add(childrenCon);
				this.mainCon.add(groupCon);
			}
			json = "["+json.substring("1")+"]";
			this.gf = eval(json);
			for(var i=0;i<data.length;i++){//第二步：加载每个分组下的好友
				this.loadChildren(childUrl,data[i]["id"]);//加载一个分组下的好友
			}
		}
		return this.mainCon;
	},
	createContainer:function(id,x,y,width,height,layout,autoScroll){//创建窗口
		return new Ext.container.Container({
			id:id,
			x:x,
			y:y,
			width:width,
			height:height,
			layout:layout
		});
	},
	createGroupHead:function(id,width,height,text){//创建好友容器的头部
		var obj = this;
		return new Ext.form.Label({
			id:id,
			x:0,
			y:0,
			width:width,
			height:height,
			html:"<span id='"+id+"_icon' style='display:block;margin-top:9px;width:12px;height:11px;float:left;background:url(./img/tool/expand.gif)'></span>&nbsp;<span style='display:block;height:30px;line-height:30px;float:left;'>&nbsp;"+text+"</span><span style='display:block;height:30px;line-height:30px;float:left;' id='"+id+"_count'>(0/0)</span>",
			style:{
				cursor:"pointer",
				display:"block"
			},
			listeners:{
				click: {
		            element: 'el',
		            fn: function(e){//单击的监听事件
				          var tid = id.substring(id.indexOf("_")+1);
				          var data = obj.getGroup(tid);
				          if(data[1]==0){//展开节点
				          	 if(obj.gf[data[0]]["load"]==1){
				          	 	 if(data[3]==30){//解决第一次打开时有问题
				          	 	 	data[3] = data[2];
				          	 	 	obj.gf[data[0]]["oldHeight"]=data[2];//替换历史高度
				          	 	 }
				          	 	 obj.mainHeight += (data[3]-30);//更改整个容器的高度属性
								 obj.mainCon.setHeight(obj.mainHeight);//更改整个容器的高度属性
					          	 obj.expand(tid);
				          	 }
				          	 obj.gf[data[0]]["flag"]=1;
				          	 document.getElementById(id+"_icon").style.background="url(./img/tool/collapse.gif)";
				          }else{
				          	if(obj.gf[data[0]]["load"]==1){
					          	obj.mainHeight -= (data[3]-30);//更改整个容器的高度属性
								obj.mainCon.setHeight(obj.mainHeight);//更改整个容器的高度属性
					          	obj.collapse(tid);
					            obj.gf[data[0]]["oldHeight"]=data[2];//替换历史高度
				            }
				            obj.gf[data[0]]["flag"]=0;
				            document.getElementById(id+"_icon").style.background="url(./img/tool/expand.gif)";
				          }
				     }
   				}
			}
		});
	},
	expand:function(id){//展开一个节点
		Ext.getCmp("groupChild_"+id).show();
		Ext.getCmp("groupContainer_"+id).setSize(this.groupWidth,this.getGroup(id)[2]);
	},
	collapse:function(id){//收缩一个节点
		Ext.getCmp("groupChild_"+id).hide();
		Ext.getCmp("groupContainer_"+id).setSize(this.groupWidth,30);
	},
	loadChildren:function(url,id){//加载一个分组下的好友
		var obj = this;
		Ext.Ajax.request({
			url:url,
			params:{
				gid:id
			},
			method:"post",
			success:function(response,opts){
				var data = eval(response.responseText);
				if(typeof(data)=="object"&&data.length>0){
					var group = obj.getGroup(id);
					obj.gf[group[0]]["height"]+=data.length*50;//更改一个分组容器的高度
					obj.gf[group[0]]["load"]=1;;//更改分组的加载状态
					Ext.getCmp("groupChild_"+id).setHeight(data.length*50);//更改子节点容器的高度
					Ext.getCmp("groupChild_"+id).hide();//隐藏子节点容器的高度
					var onlineNum=0;
					for(var i=0;i<data.length;i++){
						if(data[i]["online"]==1) onlineNum++;//将在线人数++
						Ext.getCmp("groupChild_"+id).add(obj.createChildLabel(data[i]["id"],obj.groupWidth-30,data[i]["icon"],data[i]["name"],data[i]["signature"],data[i]["online"]));
					}
					document.getElementById("groupHead_"+id+"_count").innerHTML="("+onlineNum+"/"+data.length+")";//更改在线人数统计
				}
			}
		});
	},
	createChildLabel:function(id,width,icon,name,signature,online){//创建一个用户的信息的container
		var sicon = parseInt(online)==1?"./img/info/"+icon+".jpg":"./img/info/"+icon+"_h.jpg";
		var minSIcon =  parseInt(online)==1?"./img/info/"+icon+"_m.jpg":"./img/info/"+icon+"_m_h.jpg";
		return new Ext.container.Container({
			id:"cu_"+id,
			width:width,
			height:50,
			layout:"absolute",
			items:[
				{//显示图像的label
					xtype:"label",
					x:5,
					y:5,
					width:40,
					height:40,
					html:"<img src='"+sicon+"' style='width:40px;height:40px;'/>",
					style:{
						cursor:"pointer"
					},
					listeners:{
						dblclick:{//双击事件
							element:"el",
							fn:function(){
								ListWindowHandler.openChatWin(id,name,minSIcon);
							}
						}
					}
				},{//显示名字的label
					xtype:"label",
					x:50,
					y:5,
					width:width-50,
					height:20,
					html:name
				},{//显示签名的label
					xtype:"label",
					x:50,
					y:25,
					width:width-50,
					height:20,
					html:signature
				}
			],
			listeners:{
				mouseover:{//当鼠标移动到用户的label上时，将当前label背景颜色改变
					element:"el",
					fn:function(e){
						document.getElementById("cu_"+id).style.backgroundColor="#CCE2F6";
					}
				},
				mouseout:{//当鼠标移动开用户的label上时，将当前label背景颜色还原
					element:"el",
					fn:function(e){
						document.getElementById("cu_"+id).style.backgroundColor="";
					}
				}
			}
		});
	},
	getGroup:function(id){//根据ID查找一个组的展开与收缩的FLAG
		for(var i=0;i<this.gf.length;i++){
			if(this.gf[i]["id"]==id){
				return [i,this.gf[i]["flag"],this.gf[i]["height"],this.gf[i]["oldHeight"]];
			}
		}
		return null;
	}
});
Ext.define("ZTree",{
	extend:"ZTreeBase",
	config:{
		method:"post",
		url:"/",
		childUrl:"/",
		data:[]
	},
	constructor:function(config){
		this.initConfig(config);
 				return this;
 			},
 			loadGroup:function(cmp,id,x,y,width,height){
 				var obj = this;
 				Ext.Ajax.request({
  				url:this.config.url,
  				method:this.config.method,
  				success:function(response,opts){
  					var data = eval(response.responseText);
  					cmp.removeAll();
  					cmp.add(obj.createGroup(obj.childUrl,data,id,x,y,width,height));
  				}
 				});
 			}
});