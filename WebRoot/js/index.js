var desMenu;
var navMenu;

Ext.onReady(function() { 
	if(!Ext.isWebKit||!WebsocketService.checkSupport()){
		window.location="/downloadChrome.html";
		return;
	}
	App.initDesktop();
	window.onbeforeunload=function(){
		return "确定离开Bettle?";
	}
	Ext.EventManager.onWindowResize(DesktopService.resizeBody);
});
