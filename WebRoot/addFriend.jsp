<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
<title>添加好友第一步</title>
<style>
* {
	margin: 0px;
	padding: 0px;
}

.main {
	width: 460px;
	height: 300px;
	/*border: 1px solid black;*/
	background: #fff;
	overfloat:hidden;
}

.left {
	width: 100px;
	height: 260px;
	float: left;
	/*border:1px solid red;*/
	background: -webkit-linear-gradient(#CBE1EE, #fff)
}

.right {
	width: 360px;
	height: 260px;
	float: left;
}

.bottom {
	width: 460px;
	height: 40px;
	float: left;
	background: #CBE1EE;
	text-align: right;
	border-radius: 3px;
}

.imgDiv {
	padding: 30px;
}

.nameDiv {
	font-weight: bold;
}

.common {
	padding-left: 10px;
	padding-top: 5px;
	font-size: 13px;
}

.grayText {
	color: gray;
}

.form {
	font-family: 微软雅黑;
	font-size: 14px;
	margin-left: 10px;
	height: 30px;
	line-height: 30px;
}

.success {
	font-family: 微软雅黑;
	font-size: 14px;
	background: -webkit-linear-gradient(#CBE1EE, #fff);
	height: 260px;
	line-height: 255px;
	text-align: center;
}
</style>
<script type="text/javascript">
	var id = ${param.id},bid='${param.bid}';
	$(function() {
		window.parent.ListWindowHandler.addFriend(bid);
		$.post("/servlet/LoadGroup.do", {
			uid : id
		}, function(data, sta) {
			if (typeof (data) == "object") {
				var html ="";
				for ( var i = 0; i < data.length; i++) {
					html+="<option value='"+data[i].id+"'>"+data[i].groupname+"</option>";
				}
				$("#selectGroup").html(html);
			}
		});
	});
	
	function queryAdd(){
		var param = {
				uid:${param.uid},
				gid:$("#selectGroup").val(),
				mark:$("#userMark").val()
		}
		$.post("/servlet/QueryAdd.do",param,function(data,sta){
				if((sta+"")=="success"){
					var html ="<div class='success'><img src='/img/tool/regisSucc.png'/>添加好友<font color='red'>${param.nickname}</font>成功!耐心等待他(她)的同意吧!</div>";
					html +="<div class='bottom'><button type='button' style='width: 80px; height: 25px; margin: 7px; cursor: pointer;' onclick='cancelAdd()'>关闭窗口</button></div>";
					$("#mainDiv").html(html);
				}
			});
	}
	
	function cancelAdd(){
		window.parent.ListWindowHandler.closeBrowser(bid);
	}
</script>
</head>
<body>
	<div class="main" id="mainDiv">
		<div class="left">
			<div class="imgDiv">
				<img src='img/info/${param.icon}.jpg' />
			</div>
			<div class="common nameDiv">${param.nickname}</div>
			<div class="common">${param.uid}</div>
			<div class="common grayText">${param.sex}</div>
			<div class="common grayText">${param.signature}</div>
		</div>
		<div class="right">
			<div class="form">
				备注名称 :&nbsp;<input type="text" name="mark" id="userMark"
					style="width: 180px;" />
			</div>
			<div class="form">
				分&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;组:&nbsp;<select
					id="selectGroup" style="width: 185px;">
				</select>
			</div>
		</div>
		<div class="bottom">
			<button type="button"
				style="width: 80px; height: 25px; margin-top: 7px; margin-right: 7px; cursor: pointer;"
				onclick="queryAdd()">加为好友</button>
			<button type="button"
				style="width: 60px; height: 25px; margin-top: 7px; margin-right: 7px; cursor: pointer;"
				onclick="cancelAdd()">取消</button>
		</div>
	</div>
</body>
</html>