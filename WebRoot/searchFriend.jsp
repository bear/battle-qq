<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="js/JSExtend.js"></script>
<title>好友查找页面</title>
<style>
body {
	font-family: 微软雅黑;
	background-color: #E9F0F6;
}

* {
	margin: 0px;
	padding: 0px;
}

.main {
	width: 747px;
	height: 400px;
}

.condition {
	display: block;
	margin-top: 15px;
	width: 100px;
	height: 25px;
	line-height: 25px;
	font-size: 13px;
	float: left;
	margin-left: 20px;
	cursor: pointer;
	width: 100px;
}

.checked {
	background-color: #85A3C4;
	border-radius: 5px;
	color: white;
}

.unchecked {
	color: black;
}

.resultText {
	clear: both;
	text-align: left;
	height: 50px;
	line-height: 50px;
}

#conditionDIV {
	height: 60px;
	width: 747px;
	background-color: #BFD4E8;
}

#inputDIV {
	height: 60px;
	width: 747px;
	background-color: #BFD4E8;
}

#searchResultDIV {
	width: 720px;
	margin-left: 5px;
	margin-top: 10px;
	overflow: auto;
	height: 300px;
}

.result {
	margin-top: 10px;
	margin-left: 10px;
	margin-right: 10px;
	font-family: 宋体;
	font-size: 13px;
}

.result td {
	width: 230px;
}

.underBorder {
	border-bottom: 1px solid #BFD4E8;
}

.leftBorder {
	border-left: 1px solid #BFD4E8;
}

.topBorder {
	border-top: 1px solid #BFD4E8;
}

.user_icon {
	padding: 5px;
	width: 40px;
	height: 40px;
	float: left;
}

.user_common {
	line-height: 20px;
	height: 20px;
}

.pagenation {
	width: 150px;
	float: right;
	height: 30px;
	line-height: 30px;
	font-size: 12px;
	font-family: 宋体;
	height: 30px;
}
</style>
<script type="text/javascript">
	var p = 1, total = 0, totalPage = 0, size = 12;
	var bid = '${param.bid}';
	$(function() {
		var cSpans = $("#conditionDIV span");
		cSpans.each(function() {
			$(this).click(function(e) {
				cSpans.each(function() {
					if ($(this).hasClass("checked")) {
						$(this).removeClass("checked");
						$(this).addClass("unchecked");
					}
				});
				$(this).removeClass("unchecked");
				$(this).addClass("checked");
			});
		});
	});

	/**
	 * 显示好友查询结果时，当鼠标移动到好友上时的处理函数
	 */
	 function userMessOver(id) {
		$("#user_" + id).css("background", "#CFE6F5");
		$("#mess_" + id).css("display", "none");
		$("#add_" + id).css("display", "block");
	}

	 /**
	  * 显示好友查询结果时，当鼠标移出好友上时的处理函数
	  */
	function userMessOut(id) {
		$("#user_" + id).css("background", "transparent");
		$("#mess_" + id).css("display", "block");
		$("#add_" + id).css("display", "none");
	}

	/**
	 *添加好友
	 *@param {long} uid 用户ID
	 *@param {String} nickname 昵称
	 *@param {int} icon 好友图标
	 *@param {String} sex 性别
	 *@param {String} 个性签名
	 */
	function addFriend(uid, nickname, icon, sex, sig) {
		var param = {
				uid:${param.id},
				fid:uid
		};
		$.post("/servlet/CheckFriend.do",param,function(data,sta){
			if(eval(data)==true){
				$("#noticeDIV").html("用户&nbsp;<font color='green'>"+nickname+"</font>&nbsp;已经是您的好友了，请不要重复添加!");
			}else{
				$("#hideId").val(uid);
				$("#hideName").val(nickname);
				$("#hideIcon").val(icon);
				$("#hideSex").val(sex);
				$("#hideSig").val(sig);
				$("#addFriendForm").submit();
			}
		});
	}

	 /**
	  *查找按钮的响应函数，用来查找好友
	  */
	function search() {
		if($("#conditionVal").val().trim()==0){
			$("#noticeDIV").html("查询条件不能为空!");
			return;
		}
		$("#searchResultDIV").css("line-height", "280px");
		$("#searchResultDIV").html("正在查找请稍候...");
		$
				.post(
						"/servlet/SearchFriend.do",
						{
							type : $("#conditionType").val(),
							condition : $("#conditionVal").val(),
							page : p
						},
						function(data, sta) {
							if (typeof (data) == "object") {
								var t = data.total;
								total = t;
								data = data.rows;
								totalPage = total % size == 0 ? total / size
										: parseInt(total / size) + 1;
								if (data.length == 0) {
									$("#searchResultDIV").html(
											"对不起，没有查找到好友，请尝试更换条件重新查找！");
									return;
								}
								var result = createPagenation();
								result += "<table class='result' align='left' cellpadding='0' cellspacing='0'>";
								var td = "";
								for ( var i = 0; i < data.length; i = i + 3) {
									result += "<tr>";
									if (i == 0) {
										td = "<td>";
									} else {
										td = "<td class='topBorder'>";
									}
									result += td
											+ createOneUser(data[i].icon,
													data[i].nickname,
													data[i].uid, data[i].sex,
													data[i].signature)
											+ "</td>";
									if (data[i + 1] != null
											&& data[i + 1] != undefined) {
										td=i==0?"<td class='leftBorder'>":"<td class='leftBorder topBorder'>";
										result += td
												+ createOneUser(
														data[i + 1].icon,
														data[i + 1].nickname,
														data[i + 1].uid,
														data[i + 1].sex,
														data[i + 1].signature)
												+ "</td>";
									} else {
										td=i==0?"<td>":"<td class='leftBorder topBorder'>";
										result += td + "</td>";
									}
									if (data[i + 2] != null
											&& data[i + 2] != undefined) {
										td=i==0?"<td class='leftBorder'>":"<td class='leftBorder topBorder'>";
										result += td
												+ createOneUser(
														data[i + 2].icon,
														data[i + 2].nickname,
														data[i + 2].uid,
														data[i + 2].sex,
														data[i + 2].signature)
												+ "</td>";
									} else {
										td=i==0?"<td>":"<td class='leftBorder topBorder'>";
										result += td+"</td>";
									}
									result += "</tr>";
								}
								result += "</table>";
								$("#searchResultDIV").css("border",
										"1px solid #55A8DF");
								$("#searchResultDIV").css("box-shadow",
								"-2px 2px 3px #aaa");
								$("#searchResultDIV").html(result);
							}
						});

	}

	 /**
	  *用来生成一个好友信息的DIV
	  *
	  *@param {int} icon 个性图像编号
	  *@param {String} name 用户昵称
	  *@param {long} uid 用户ID
	  *@param {String} sex 用户性别
	  *@param {sig} sig 个性签名
	  */
	function createOneUser(icon, name, uid, sex, sig) {
		var html = "<div id='user_"
				+ uid
				+ "' style='width:230px;height:60px;cursor:pointer;' onmouseover='userMessOver("
				+ uid + ")' onmouseout='userMessOut(" + uid + ")'>";
		html += "<div class='user_icon'><img src='img/info/"+icon+".jpg'/></div>";
		html += "<div id='mess_"+uid+"'>";
		html += "<div class='user_common'>" + (name + "(" + uid).ellipsis(28)
				+ ")</div>";
		//html += "<div class='user_common'>" + sex + "</div>";
		html += "<div class='user_common'>" + sig.ellipsis(20) + "</div>";
		html += "</div>";
		html += "<div id='add_"+uid+"' style='display:none;height:60px;line-height:60px;'><img src='img/tool/addfriend.png' style='margin-left:20px;margin-top:15px;'  title='加为好友' onclick='addFriend(\""
				+ uid
				+ "\",\""
				+ name
				+ "\",\""
				+ icon
				+ "\",\""
				+ sex
				+ "\",\"" + sig + "\")'/></div>";
		html += "</div>";
		return html;
	}

	/**
	 *生成翻页用户的DIV
	 *
	 */
	function createPagenation() {
		var html = "<div align='right' class='pagenation'>";
		html += "<div style='width:100px;height:30px;text-align:right;line-height:30px;float:left;'>";
		html += "当前第<font color='blue'>" + p + "</font>/" + totalPage + "页";
		html += "</div>";
		html += "<div style='width:16px;height:20px;line-height:30px;float:left;margin-top:7px;'>";
		if (p == 1) {
			html += "<img src='img/tool/page-prev-dis.gif'/>";
		} else {
			html += "<img src='img/tool/page-prev.gif' style='cursor:pointer;' title='上一页' onclick='pagePrev()'/>";
		}
		html += "</div>";
		html += "<div style='width:16px;height:20px;line-height:30px;float:left;margin-top:7px;'>";
		if (p == totalPage) {
			html += "<img src='img/tool/page-next-dis.gif'/>";
		} else {
			html += "<img src='img/tool/page-next.gif' style='cursor:pointer;' title='下一页' onclick='pageNext()'/>";
		}
		html += "</div>";
		html += "</div>";
		return html;
	}

	/**
	 *上一页响应函数
	 */
	function pagePrev() {
		p--;
		search();
	}

	/**
	 *下一页响应函数
	 */
	function pageNext() {
		p++;
		search();
	}
</script>
</head>
<body>
	<input type="hidden" id="conditionType" value="0" />
	<div align="center" class="main">
		<div align="center" id="conditionDIV">
			<div style="width: 450px;">
				<span class="condition checked">精确查找</span> <span
					class="condition unchecked">按条件查找</span>
			</div>
		</div>
		<div align="center" id="inputDIV">
			<div style="width: 450px;">
				<div style="width: 300px; float: left;">
					<input type="text"
						style="width: 300px; height: 25px; border-radius: 5px;"
						id="conditionVal" />
				</div>
				<div
					style="cursor: pointer; width: 100px; height: 30px; margin-left: 10px; background: -webkit-linear-gradient(#9BB8D3, #6799D2); border-radius: 5px; float: left;"
					onmousedown="javascript:this.style.background='-webkit-linear-gradient(#6799D2,#9BB8D3)'"
					onmouseup="javascript:this.style.background='-webkit-linear-gradient(#9BB8D3,#6799D2)'"
					onclick="search()">
					<div
						style="width: 30px; height: 30px; margin-left: 10px; float: left; background: transparent; background: url(img/tool/searchMiddle.png)">
					</div>
					<span id="searchButton"
						style="width: 60px; height: 30px; display: block; float: left; line-height: 30px; text-align: left; color: #fff;">查找</span>
				</div>
				<div
					style="clear: both; text-align: center; color: red; font-size: 14px;"
					id="noticeDIV"></div>
			</div>
		</div>
		<div id="searchResultDIV"></div>
	</div>
	</table>
	<form action="/addFriend.jsp" method="post" id="addFriendForm">
		<input type="hidden" name="uid" id="hideId" /> <input type="hidden"
			name="nickname" id="hideName" /> <input type="hidden" name="icon"
			id="hideIcon" /> <input type="hidden" name="sex" id="hideSex" /> <input
			type="hidden" name="signature" id="hideSig" /> <input type="hidden"
			name="id" value="${param.id}" /> <input type="hidden" name="bid"
			value="${param.bid}" />
	</form>
</body>
</html>