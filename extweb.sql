﻿# MySQL-Front 5.1  (Build 4.2)

/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE */;
/*!40101 SET SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES */;
/*!40103 SET SQL_NOTES='ON' */;


# Host: 127.0.0.1    Database: extweb
# ------------------------------------------------------
# Server version 5.5.27

USE `extweb`;

#
# Source for table _friends
#

CREATE TABLE `_friends` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `gid` int(11) NOT NULL DEFAULT '1234' COMMENT '分组ID',
  `fuid` bigint(15) NOT NULL DEFAULT '1234' COMMENT '好友UID',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='好友表';

#
# Source for table _group
#

CREATE TABLE `_group` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(15) NOT NULL DEFAULT '1234' COMMENT '用户UID，与users的UID关联',
  `groupname` varchar(50) NOT NULL DEFAULT '我的好友' COMMENT '好友分组名称',
  PRIMARY KEY (`Id`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='好友分组';

#
# Source for table _history
#

CREATE TABLE `_history` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `user` bigint(15) DEFAULT NULL COMMENT '用户一',
  `auser` bigint(15) DEFAULT NULL COMMENT '用户二(other user)',
  `date` date DEFAULT NULL COMMENT '记录日期',
  `record` longtext COMMENT '记录',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='历史记录';

#
# Source for table _offlinemsg
#

CREATE TABLE `_offlinemsg` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL DEFAULT '0' COMMENT '接收人ID',
  `date` date DEFAULT NULL COMMENT '发送日期',
  `msg` text COMMENT '聊天信息',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='存储离线消息';

#
# Source for table _users
#

CREATE TABLE `_users` (
  `uid` bigint(11) NOT NULL DEFAULT '1234' COMMENT '用户的UID，由程序随机生成',
  `password` varchar(128) DEFAULT NULL COMMENT '密码',
  `mail` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `sex` varchar(4) NOT NULL DEFAULT '男' COMMENT '性别',
  `nickname` varchar(50) NOT NULL DEFAULT '暂无' COMMENT '真实姓名',
  `online` int(1) NOT NULL DEFAULT '0' COMMENT '用户状态，0 不在线 1在线; 2 忙碌; 3离线; 4 隐身',
  `icon` varchar(10) NOT NULL DEFAULT '1' COMMENT '用户图像',
  `signature` varchar(255) DEFAULT NULL COMMENT '签名',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
